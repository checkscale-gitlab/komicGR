const path = require("path");
const commonConfig = require("./webpack.common");
const { merge } = require("webpack-merge");
const CopyPlugin = require("copy-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

prodConfig = {
  mode: "production",
  plugins: [
    new CleanWebpackPlugin(),
    new CopyPlugin({
      patterns: [{ from: "package.json" }, { from: "yarn.lock" }],
    }),
  ],
  output: {
    path: path.resolve(__dirname, "..", "build"),
    filename: "app.js",
  },
};

module.exports = merge(commonConfig, prodConfig);
