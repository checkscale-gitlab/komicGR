const path = require("path");
const nodeExternals = require("webpack-node-externals");

module.exports = {
  entry: path.join(__dirname, "..", "src", "app.ts"),
  target: "node",
  externals: [nodeExternals()],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        options: {
          configFile: "tsconfig.json",
        },
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".js"],
  },
};
