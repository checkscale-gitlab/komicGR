// 'use strict';

process.env.NODE_ENV = "test";
process.env.confirmEmailSecret = "test";
process.env.TS_NODE_PROJECT = "./tsconfig.spec.json";
process.env.TS_NODE_LOG_ERROR = true;

module.exports = {
  extension: ["ts"],
  spec: "src/Test",
  require: ["reflect-metadata/Reflect", "ts-node/register", "dotenv/config"],
  watch: true,
  "watch-files": ["src/Test/**/*.spec.ts"],
  recursive: true,
};
