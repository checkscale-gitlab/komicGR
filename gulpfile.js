
const gulp = require('gulp');
const del = require('del');
const fs = require('fs');

async function cleanOlderCompiledTests() {
  const deletePaths = await del(['./tests']);
  console.log('Deleted files and folders:\n', deletePaths.join('\n'));
}

async function createTestsFolder() {
  fs.mkdirSync('./tests');
}

exports.default = gulp.series(cleanOlderCompiledTests, createTestsFolder)