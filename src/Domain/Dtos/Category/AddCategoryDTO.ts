interface AddCategoryDTO {
    Name: string,
    Description: string
}

export default AddCategoryDTO;