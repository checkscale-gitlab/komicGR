import Tag from "../../Models/Tag/Tag";

interface PostResponseDTO {
    id: number,
    title: string,
    user_id: number,
    short_body: string,
    body: string,
    likes: number,
    createdAt: string,
    imageUrl: string,
    profileImageUrl: string,
    tags: Tag[],
    categoryId?: number
}

export default PostResponseDTO;