interface EditPostDTO {
    title: string,
    short_body: string
    body: string,
    imageUrl: string,
    categoryId: number 
}

export default EditPostDTO;
