import Role from "../../Models/Role/Role";

interface ProfileUserResponseDTO {
    id: number,
    role: Role,
    first_name: string,
    last_name: string,
    email: string,
    username: string,
    job_desc: string,
    address: string,
    mobile: number,
    profile_image_url: string,
    registration_date: string
}

export default ProfileUserResponseDTO;

export const ProfileUserComponent = {
    type: "object",
    properties: {
        id: {
            type: "integer",
            example: parseInt('1')
        },
        role: {
            type: "string",
            enum: ["God", "Superhuman", "Human"],
            example: "Human"
        },
        first_name: {
            type: "string",
            example: "Joe"
        },
        last_name: {
            type: "string",
            example: "banana"
        },
        email: {
            type: "string",
            example: "bananajoe@email.com"
        },
        username: {
            type: "string",
            example: "BJoe"
        },
        job_desc: {
            type: "string",
            example: "Delivers bananas"
        },
        address: {
            type: "string",
            example: "amantido"
        },
        mobile: {
            type: "integer",
            example: parseInt("6934123456")
        },
        profile_image_url: {
            type: "string",
            example: "/path/to/image"
        },
        registration_date: {
            type: "string",
            example: "Sat Jun 20 2020"
        }
    }
};