interface RegisterUserDTO {
    first_name: string,
    last_name: string,
    email: string,
    username: string,
    password: string,
    job_desc: string,
    address: string,
    mobile: number
}

export default RegisterUserDTO;

export const RegisterUserComponent = {
    type: "object",
    properties: {
        first_name: { type: "string", example: "Joe" },
        last_name: { type: "string", example: "Doe" },
        email: { type: "string", example: "joe@doe.com" },
        username: { type: "string", example: "joe" },
        password: { type: "string", example: "password" },
        job_desc: { type: "string", example: "software developer" },
        address: { type: "string", example: "sesame str" },
        mobile: { type: "number", example: parseInt("6934123456") }
    }
}