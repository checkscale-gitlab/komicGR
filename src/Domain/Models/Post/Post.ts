import PostResponseDTO from "../../Dtos/Post/PostResponseDTO";
import Tag from "../Tag/Tag";

class Post {

    // TODO: Improve this constructor in order to improve the
    // instantiation of a post
    constructor(
        public id: number,
        public title: string,
        public short_body: string,
        public body: string,
        public likes: number,
        public user_id: number,
        public created_at: string,
        public image_url: string,
        public profile_image_url: string,
        public tags: Tag[] = [],
        public category_id?: number
    ) {
        if (!title)
            throw new Error("A post should have a title");

        if (!user_id)
            throw new Error("A post should have a user id");

        if (!body)
            throw new Error("A post should have a body text");

        if (!image_url)
            throw new Error("A post should have an image url");

        if (!profile_image_url)
            throw new Error("A post should have the user's image url");

        if (!likes)
            this.likes = 0;

        if (!short_body)
            this.short_body = this.body.substring(0, 20);
    }

    static toPostResponse(post: Post): PostResponseDTO {
        return {
            id: post.id,
            title: post.title,
            user_id: post.user_id,
            short_body: post.short_body,
            body: post.body,
            likes: post.likes,
            createdAt: post.created_at,
            imageUrl: post.image_url,
            tags: post.tags,
            categoryId: post.category_id || -1,
            profileImageUrl: post.profile_image_url // For now this is only used when all the posts are requested
        }
    }

    static toPostArrayResponse(posts: Post[]): PostResponseDTO[] {
        let postResponsesArray: PostResponseDTO[] = [];

        for (let post of posts) {
            postResponsesArray.push(Post.toPostResponse(post));
        }

        return postResponsesArray;
    }

    static createPostFromQueryResult = (result: { id: string, title: string, short_body: string, body: string, likes: string, user_id: string, created_at: string, image_url: string, profile_image_url: string, tags: string, category_id: string }) => new Post(
        +result.id,
        result.title,
        result.short_body,
        result.body,
        +result.likes,
        +result.user_id,
        result.created_at,
        result.image_url,
        result.profile_image_url,
        Tag.createTagsFromQueryResult(result.tags),
        +result.category_id
    );
}

export default Post;