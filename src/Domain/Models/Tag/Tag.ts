const SEPARATOR = '##'
class Tag {
  constructor(public id: number, public name: string) {
    if (name.length === 0) throw new Error("A tag name should not be empty");
  }

  static createTagsFromQueryResult = (tagsString: string) => {
    const tags: Tag[] = []
    const splittedTags = tagsString.split(SEPARATOR);

    splittedTags.forEach((t: string) => {
      const tag = JSON.parse(t) as { id: string, name: string };
      tags.push(new Tag(+tag.id, tag.name))
    });

    return tags;
  }
}

export default Tag;
