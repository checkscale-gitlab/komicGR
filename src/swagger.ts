import { ROUTES } from './constants';
import {
  UsersComponents as UsersComponentsV1,
  UsersPaths as UsersPathsV1
} from './WebApi/Routes/v1/User/UserRoutes.docs';
import {
  UsersComponents as UsersComponentsV2,
  UsersPaths as UsersPathsV2
} from './WebApi/Routes/v2/User/UserRoutes.docs';

const swaggerDocument = {
  openapi: '3.0.1',
  info: {
    version: '1.0.0',
    title: 'Blog Api Documentation',
    description: 'This is the documentation for the Blog Api',
    termsOfService: '',
    contact: {
      name: process.env.SWAGGER_CONTACT_NAME,
      email: process.env.SWAGGER_CONTACT_MAIL,
      url: process.env.APPLICATION_URL
    },
    license: {
      name: 'MIT'
    }
  },
  components: {
    securitySchemes: {
      bearerAuth: {
        type: 'apiKey',
        in: 'header',
        name: 'Authorization',
        description: 'Auth token prefixed with JWT'
      }
    }
  }
};

const getServers = (apiVersion: string) => [
  {
    url: '{protocol}://{host}:{port}/api/{version}',
    variables: {
      host: {
        default: 'localhost'
      },
      protocol: {
        enum: ['http', 'https'],
        default: 'http'
      },
      port: {
        default: process.env.PORT || 3001
      },
      version: {
        default: apiVersion
      }
    }
  }
];

const apiV1 = {
  ...swaggerDocument,
  servers: getServers(ROUTES.API_VERSION.V1),
  paths: {
    ...UsersPathsV1
  },
  components: {
    ...swaggerDocument.components,
    ...UsersComponentsV1
  }
};

const apiV2 = {
  ...swaggerDocument,
  servers: getServers(ROUTES.API_VERSION.V2),
  paths: {
    ...UsersPathsV2
  },
  components: {
    ...swaggerDocument.components,
    ...UsersComponentsV2
  }
};
export { apiV1, apiV2 };
