import ITagService from "../../../Application.Services/Tag/ITagService";
import { Request, Response } from "express";
import Tag from "../../../Domain/Models/Tag/Tag";

export async function handleGetTags(
  _: Request,
  __: Response,
  tagService: ITagService
): Promise<Tag[]> {
  const tags = await tagService.getTags();

  return tags;
}
