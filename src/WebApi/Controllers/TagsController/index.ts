export { handleGetTags } from './GetTagsRequestHandler';

// NOTE: DUE TO THE FACT THAT SINON(v4) CANNOT STUB FUNCTIONS WITHOUT
// IMPORTING using the `import * as` SYNTAX, YOU SHOULD NOT IMPORT
// THESE HANDLERS FROM THIS INDEX FILE IN THE TESTS OTHERWISE THE STUBS
// WILL NOT WORK AS INTENDED. INSTEAD IMPORT DIRECTLY FROM EACH MODULE
// AT LEAST UNTILL SINON IS UPDATED AND THEN WE CAN USE FAKES TO MOCK
// FUNCTIONS. YOU CAN USE THIS INDEX FILE FOR THE PRODUCTION CODE
