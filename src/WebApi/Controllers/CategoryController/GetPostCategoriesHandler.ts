import ICategoryService from "../../../Application.Services/Category/ICategoryService";
import { Request, Response } from "express";
import Category from "../../../Domain/Models/Category/Category";

export async function handleGetPostCategories(req: Request, res: Response, categoryService: ICategoryService): Promise<{ success: boolean, categories: Category[] }> {    
    const categoriesResult = await categoryService.getPostCategories();
    
    return { success: true, categories: categoriesResult };
}