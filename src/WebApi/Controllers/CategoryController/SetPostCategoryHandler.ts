import SetCategoryDTO from "../../../Domain/Dtos/Category/SetCategoryDTO";
import IPostService from "../../../Application.Services/Post/IPostService";
import ICategoryService from "../../../Application.Services/Category/ICategoryService";
import { Request, Response } from "express";
import Role from "../../../Domain/Models/Role/Role";

export async function handleSetPostCategory(req: Request, res: Response, categoryService: ICategoryService, postService: IPostService): Promise<{ success: boolean, msg: string }> {
    const setCategoryDTO = <SetCategoryDTO>req.body;
    const post = await postService.getPostByPostId(setCategoryDTO.Post_id);

    if (post.user_id != req.userFromPassport.id) {
        throw { message: 'You are not allowed to set this post\'s category' };
    }

    if (req.userFromPassport.role === Role.Human) {
        throw { message: "You are not authorized for this action" }
    }

    await categoryService.setPostCategory(setCategoryDTO.Post_id, setCategoryDTO.Category_id);
    return { success: true, msg: 'Post Category Set' };
}