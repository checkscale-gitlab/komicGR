import AddCategoryDTO from "../../../Domain/Dtos/Category/AddCategoryDTO";
import Role from "../../../Domain/Models/Role/Role";
import ICategoryService from "../../../Application.Services/Category/ICategoryService";
import { Request, Response } from "express";

export async function handleAddCategory(req: Request, res: Response, categoryService: ICategoryService): Promise<{ success: boolean, msg: string }> {
    const newCategory = <AddCategoryDTO>req.body;
    const user = req.userFromPassport;

    if (Role[user.role.toString()] === Role[Role.Human]) {
        throw { message: 'You are not authorized for this action' };
    }

    await categoryService.addCategory(newCategory)

    return { success: true, msg: "Category added" };
}