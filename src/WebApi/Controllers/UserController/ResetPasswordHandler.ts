import { Response, Request } from "express";
import IUserService from "../../../Application.Services/User/IUserService";

export async function handleResetPasswordRequest(req: Request, res: Response, userService: IUserService): Promise<{ success: boolean }> {
    const userId = req.userFromPassport.id;
    const newPassword = req.body.newPassword;

    if (newPassword.length < 8) {
        throw new Error('A password must have at least 8 characters');
    }

    const response = await userService.updatePassword(userId, newPassword);

    // TODO: send an email to notify the user that the password has been reset.
    // Also the user should be logged out the next time (s)he visits the app

    return { success: response };
}