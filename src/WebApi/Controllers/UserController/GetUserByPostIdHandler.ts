import User from "../../../Domain/Models/User/User";
import IUserService from "../../../Application.Services/User/IUserService";
import { Request, Response } from "express";
import ProfileUserResponseDTO from "../../../Domain/Dtos/User/ProfileUserResponseDTO";

export async function handelGetUserByPostId(req: Request, res: Response, userService: IUserService): Promise<{ success: boolean, user: ProfileUserResponseDTO }> {
    const user = await userService.getUserByPostId(+req.params.postId);

    return { success: true, user: User.toProfileResponse(user) };
}