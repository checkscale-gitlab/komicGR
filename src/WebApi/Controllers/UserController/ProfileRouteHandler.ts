import { Request, Response } from "express";
import ProfileUserResponseDTO from "../../../Domain/Dtos/User/ProfileUserResponseDTO";

export function handleProfileRequest(req: Request, res: Response): { user: ProfileUserResponseDTO } {
    return { user: { ...req.userFromPassport, registration_date: req.userFromPassport.registration_date.toString() } };
}