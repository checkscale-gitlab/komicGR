import IUserService from "../../../Application.Services/User/IUserService";
import { Request, Response } from "express";

export async function handleConfirmEmailRequest(
  req: Request,
  res: Response,
  userService: IUserService,
  jwt: any
): Promise<string> {
  const verified = jwt.verify(req.params.token, process.env.confirmEmailSecret);
  const userId = verified.Id;

  const confirm = await userService.setConfirmedEmail(userId, true);

  if (confirm) {
    return process.env.APPLICATION_URL ?? '';
  } else {
    return process.env.APPLICATION_ERROR_PAGE ?? '';
  }
}
