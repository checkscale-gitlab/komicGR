import { Request, Response } from 'express';
import INodeMailerWrapper from '../../../Helpers/NodeMailer/INodeMailerWrapper';
import MailOptions from '../../../Domain/Dtos/MailOptionsDTO';
import IUserService from '../../../Application.Services/User/IUserService';
import User from '../../../Domain/Models/User/User';

export const getResetMailOptions: (email: string, token: string) => MailOptions = (email: string, token: string) => ({
    from: process.env.FROM_EMAIL ?? '',
    to: email,
    subject: "Reset Password",
    text: "Click this link to reset your password",
    html: `<p>You can reset your password by clicking this <a href='${process.env.APPLICATION_URL}/resetPassword/${token}'>link</a></p><i>This link will be active for 5 minutes</i>`,
});


// TODO: This handler sends the email to reset the user's password
// so it should be renamed to be more specific.
export async function handleSendEmailRequest(req: Request, res: Response, userService: IUserService, nodeMailerWrapper: INodeMailerWrapper, jwt: any): Promise<boolean> {
    const email = req.body.email;
    const user: User = await userService.getByEmail(email);

    if (user) {
        const token = jwt.sign({ "email": user.email }, process.env.secret, {
            expiresIn: '5m'
        });

        const mailOptions = getResetMailOptions(user.email, token);

        nodeMailerWrapper.sendEmail(mailOptions);

        return true;
    }

    return false;
}