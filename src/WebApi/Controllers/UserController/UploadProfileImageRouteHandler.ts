import IUserService from '../../../Application.Services/User/IUserService';
import { Request, Response } from 'express';
import multer from 'multer';


export async function handleUploadProfileImageRequest(
    req: Request,
    res: Response,
    userService: IUserService,
    multerFactory: any,
    storageOptions: multer.DiskStorageOptions,
    promisifyMulter: any
): Promise<{ success?: boolean, msg?: string, errorMessage?: string, errorStackTrace?: any }> {
    const storageOptionsClone = Object.assign({}, storageOptions);
    storageOptionsClone.destination = (storageOptions.destination! as String).concat(String(req.userFromPassport.id)).concat('/');
    const createMulterInstance = multerFactory(storageOptionsClone);

    await promisifyMulter(createMulterInstance().single('userFile'), req, res);

    const profileImageUrl = '/profile-images/' + req.userFromPassport.id + '/profile.' + req.file.originalname.split('.')[1];

    const updated = await userService.setProfileImageUrl(req.userFromPassport.id, profileImageUrl);

    return { success: updated, msg: "Profile Image added succesfully" };
}