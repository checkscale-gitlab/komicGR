import User from "../../../Domain/Models/User/User";
import IUserService from "../../../Application.Services/User/IUserService";
import { Response, Request } from "express";

export async function handleGetUserById(req: Request, res: Response, userService: IUserService) {
    const user = await userService.getById(+req.params.id);

    return User.toProfileResponse(user);
}