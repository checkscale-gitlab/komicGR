import RegisterUserDTO from '../../../Domain/Dtos/User/RegisterUserDTO';
import MailOptions from '../../../Domain/Dtos/MailOptionsDTO';
import IUserService from '../../../Application.Services/User/IUserService';
import INodeMailerWrapper from '../../../Helpers/NodeMailer/INodeMailerWrapper';
import { Request, Response } from "express";

export const getConfirmationMailOptions = (user: RegisterUserDTO, token: string) => ({
    from: process.env.FROM_EMAIL ?? '',
    to: user.email,
    subject: "Confirmation Email",
    text: "Please confirm your email by clicking the link",
    html: `<p>Please confirm your email by clicking this <a href="${process.env.BACKEND_URL}/users/confirmEmail/${token}">link</a></p><i>This link will be active for 24 hours</i>`
})

export async function handleRegisterRequest(
    req: Request, res: Response, userService: IUserService, nodeMailerWrapper: INodeMailerWrapper, jwt
): Promise<{ success: boolean, msg: string, userId?: number }> {
    const newUser = <RegisterUserDTO>req.body;

    if (newUser.username.length < 3) {
        return { success: false, msg: "Username must have at least 3 characters." };
    }

    if (newUser.password.length < 8) {
        return { success: false, msg: "Password must have at least 8 characters." };
    }

    const userByUserName = await userService.getByUsername(newUser.username);

    if (!userByUserName) {
        const userByEmail = await userService.getByEmail(newUser.email);

        if (!userByEmail) {
            const insertedUser = await userService.insertUser(newUser);

            const token = jwt.sign({ "Id": insertedUser.insertId }, process.env.confirmEmailSecret, {
                expiresIn: '1d'
            });
            const mailOptions: MailOptions = getConfirmationMailOptions(newUser, token);

            nodeMailerWrapper.sendEmail(mailOptions);

            return {
                success: true,
                msg: "User registered",
                userId: insertedUser.insertId
            };

        } else {
            throw { message: "This email already exists" };
        }
    } else {
        throw { message: "This username already exists" };
    }
}