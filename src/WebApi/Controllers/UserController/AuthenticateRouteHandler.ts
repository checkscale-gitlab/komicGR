import User from "../../../Domain/Models/User/User";
import IUserService from "../../../Application.Services/User/IUserService";
import { Request, Response } from "express";

export async function handleAuthenticateRequest(
  req: Request,
  res: Response,
  userService: IUserService,
  jwt
): Promise<{ success: boolean; msg?: string; token?: string }> {
  const username = req.body.username;
  const password = req.body.password;

  const user: User = await userService.getByUsername(username);

  if (!user) {
    throw { message: "User doesn't exist" };
  }

  if (user && !user.confirmed) {
    throw { message: "Please confirm your email" };
  }

  const isMatch = await userService.comparePassword(password, user.password);

  if (isMatch) {
    const token = jwt.sign(
      {
        Id: user.id,
      },
      process.env.secret,
      {
        expiresIn: 604800, //1 week
      }
    );
    return {
      success: true,
      token: token,
    };
  } else {
    throw { message: "Wrong Credentials" };
  }
}
