import IUserService from "../../../Application.Services/User/IUserService";
import { Response, Request } from "express";

export async function handleUploadProfileImageUrlRequest(req: Request, res: Response, userService: IUserService): Promise<{ success: boolean, msg: string }> {
    const updated = await userService.setProfileImageUrl(req.userFromPassport.id, req.body.profileImageUrl);

    return { success: updated, msg: "Profile Image Url added succesfully" };

}