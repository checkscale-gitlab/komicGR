export { handleRegisterRequest } from './RegisterRouteHandler';
export { handleAuthenticateRequest } from './AuthenticateRouteHandler';
export { handleProfileRequest } from './ProfileRouteHandler';
export { handleGetAllUsersRequest } from './GetAllUsersRequestHandler';
export { handleGetUserById } from './GetUserByIdHandler';
export { handleUploadProfileImageUrlRequest } from './UploadProfileImageUrlRouteHandler';
export { handleUploadProfileImageRequest } from './UploadProfileImageRouteHandler';
export { handelGetUserByPostId } from './GetUserByPostIdHandler';
export { handleConfirmEmailRequest } from './ConfirmEmailRequestHandler';
export { handleSendEmailRequest } from './SendEmailRequestHandler';
export { handleResetPasswordRequest } from './ResetPasswordHandler';

// NOTE: DUE TO THE FACT THAT SINON(v4) CANNOT STUB FUNCTIONS WITHOUT
// IMPORTING using the `import * as` SYNTAX, YOU SHOULD NOT IMPORT
// THESE HANDLERS FROM THIS INDEX FILE IN THE TESTS OTHERWISE THE STUBS
// WILL NOT WORK AS INTENDED. INSTEAD IMPORT DIRECTLY FROM EACH MODULE
// AT LEAST UNTILL SINON IS UPDATED AND THEN WE CAN USE FAKES TO MOCK
// FUNCTIONS. YOU CAN USE THIS INDEX FILE FOR THE PRODUCTION CODE
