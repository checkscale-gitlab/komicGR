import { Request, Response } from "express";

import IPostService from "../../../Application.Services/Post/IPostService";

import PostResponseDTO from "../../../Domain/Dtos/Post/PostResponseDTO";
import Post from "../../../Domain/Models/Post/Post";

export async function handleGetPostsByCategoryId(req: Request, res: Response, postService: IPostService): Promise<{ posts: PostResponseDTO[] }> {
    const posts = await postService.getPostsByCategoryId(+req.query.categoryId!, +req.query.offset!, +req.query.limit!)

    return { posts: Post.toPostArrayResponse(posts) };
}