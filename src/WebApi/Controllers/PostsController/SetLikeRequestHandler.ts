import { Request, Response } from 'express';

import IPostService from "../../../Application.Services/Post/IPostService";

import LikePostDTO from "../../../Domain/Dtos/Post/LikePostDTO";

export async function handleSetLikeRequest(req: Request, res: Response, postService: IPostService): Promise<{ success: boolean, msg: string }> {
    const likePostDTO = <LikePostDTO>req.body;

    const result = await postService.likePost(req.userFromPassport.id, likePostDTO.post_id, likePostDTO.like);

    return { success: result, msg: `Like set to ${likePostDTO.like} for post with post id ${likePostDTO.post_id}` };
}