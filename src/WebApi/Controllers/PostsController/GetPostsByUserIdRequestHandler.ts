import { Request, Response } from "express";

import IPostService from "../../../Application.Services/Post/IPostService";

import Post from "../../../Domain/Models/Post/Post";
import PostResponseDTO from "../../../Domain/Dtos/Post/PostResponseDTO";

export async function handleGetPostsByUserId(req: Request, res: Response, postService: IPostService): Promise<{ posts: PostResponseDTO[] }> {
    const posts = await postService.getPostsByUserId(+req.params.userId, +req.query.offset!, +req.query.limit!);

    return { posts: Post.toPostArrayResponse(posts) };
}