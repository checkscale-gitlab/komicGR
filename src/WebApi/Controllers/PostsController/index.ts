export { handleGetPostsByUserId } from './GetPostsByUserIdRequestHandler';
export { handleAddPost } from './AddPostRequestHandler';
export { handleGetAllPosts } from './GetAllPostsRequestHandler';
export { handleGetPostByPostId } from './GetPostByPostIdRequestHandler';
export { handleGetPostsByCategoryId } from './GetPostsByCategoryIdRequestHandler';
export { handleSetLikeRequest } from './SetLikeRequestHandler';
export { handleGetPostLikeStatusRequest } from './GetPostUserLikeStatus';
export { handleGetPostLikesRequest } from './GetPostLikesRequestHandler';
export { handleDeletePostRequest } from './DeletePostRequestHandler';
export { handleEditPost } from './EditPostRequestHandler';
export { handleSetPostAsUserFavourite } from './SetPostAsUserFavouriteHandler';

// NOTE: DUE TO THE FACT THAT SINON(v4) CANNOT STUB FUNCTIONS WITHOUT
// IMPORTING using the `import * as` SYNTAX, YOU SHOULD NOT IMPORT
// THESE HANDLERS FROM THIS INDEX FILE IN THE TESTS OTHERWISE THE STUBS
// WILL NOT WORK AS INTENDED. INSTEAD IMPORT DIRECTLY FROM EACH MODULE
// AT LEAST UNTILL SINON IS UPDATED AND THEN WE CAN USE FAKES TO MOCK
// FUNCTIONS. YOU CAN USE THIS INDEX FILE FOR THE PRODUCTION CODE
