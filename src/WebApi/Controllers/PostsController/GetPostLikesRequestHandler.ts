import { Request, Response } from 'express';

import IPostService from "../../../Application.Services/Post/IPostService";

export async function handleGetPostLikesRequest(req: Request, res: Response, postService: IPostService): Promise<{ likes: number }> {
    const likesCount = await postService.getNumberOfPostLikes(+req.params.postId);

    return { likes: likesCount };
}