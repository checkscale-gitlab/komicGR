import IPostService from "../../../Application.Services/Post/IPostService";
import { Request, Response } from "express";
import EditPostDTO from "../../../Domain/Dtos/Post/EditPostDTO";

export async function handleEditPost(
    req: Request,
    res: Response,
    postService: IPostService
): Promise<number> {
    const editPostDTO = req.body as EditPostDTO;
    const postId = +req.params.postId;
    const { user_id: userId } = await postService.getPostByPostId(postId);

    if (req.userFromPassport.id !== userId) {
        throw new Error("You are not authorized to edit this post");
    }

    if (!editPostDTO.title) {
        throw new Error("Required fields are missing");
    };

    const result = await postService.editPost(+postId, editPostDTO);

    return result.id;
}