import { Request, Response } from 'express';

import IPostService from "../../../Application.Services/Post/IPostService";

export async function handleGetPostLikeStatusRequest(req: Request, res: Response, postService: IPostService): Promise<{ like: boolean }> {
    const result = await postService.getIfUserLikesPostByPostId(req.userFromPassport.id, +req.params.postId);

    return { like: result };
}