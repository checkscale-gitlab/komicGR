import { Request, Response } from 'express';

import IPostService from "../../../Application.Services/Post/IPostService";

import LikePostDTO from "../../../Domain/Dtos/Post/LikePostDTO";

export async function handleSetPostAsUserFavourite(postId: number, userId: number, postService: IPostService): Promise<{ postId: number }> {
    const result = await postService.setPostAsUserFavourite(postId, userId);

    return result;
}