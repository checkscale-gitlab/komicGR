import { Request, Response } from 'express';

import IPostService from "../../../Application.Services/Post/IPostService";

import Post from "../../../Domain/Models/Post/Post";
import PostResponseDTO from "../../../Domain/Dtos/Post/PostResponseDTO";

export async function handleGetAllPosts(req: Request, res: Response, postService: IPostService): Promise<{ posts: PostResponseDTO[] }> {
    const posts = await postService.getAllPosts();
    
    return { posts: Post.toPostArrayResponse(posts) };
}