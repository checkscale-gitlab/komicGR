import { ProfileUserComponent } from "../../../../Domain/Dtos/User/ProfileUserResponseDTO";

const getAllUsers = {
    "/users/": {
        get: {
            tags: ['Users'],
            description: "Returns all the available users",
            operationId: 'getAllUsers',
            responses: {
                200: {
                    description: "All the users information.",
                    content: {
                        "application/json": {
                            schema: {
                                type: "array",
                                items: {
                                    $ref: "#/components/ProfileUser"
                                }
                            }
                        }
                    }
                }
            },
        }
    }
};


export const UsersPaths = {
    ...getAllUsers,
}

export const UsersComponents = {
    ProfileUser: {
        ...ProfileUserComponent,
    },
}