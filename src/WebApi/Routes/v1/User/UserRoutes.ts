import passport from 'passport';
import { Request, Response } from 'express';
import { controller, httpPost, httpGet } from 'inversify-express-utils';
import { inject } from 'inversify';

import {
  handleRegisterRequest,
  handleAuthenticateRequest,
  handleProfileRequest,
  handleGetAllUsersRequest,
  handleGetUserById,
  handleUploadProfileImageUrlRequest,
  handleUploadProfileImageRequest,
  handelGetUserByPostId,
  handleConfirmEmailRequest,
  handleSendEmailRequest,
  handleResetPasswordRequest
} from '../../../Controllers/UserController';
import TYPES from '../../../../Helpers/DI/Types';
import IUserService from '../../../../Application.Services/User/IUserService';
import INodeMailerWrapper from '../../../../Helpers/NodeMailer/INodeMailerWrapper';
import { handleError } from '../utils';
import { ROUTES } from '../../../../constants';

@controller(`/${ROUTES.API_VERSION.V1}/users`)
export class UserRouteV1 {
  constructor(
    @inject(TYPES.IUserService) private userService: IUserService,
    @inject(TYPES.INodeMailerWrapper)
    private nodeMailerWrapper: INodeMailerWrapper,
    @inject(TYPES.JWT) private jwt: unknown,
    @inject(TYPES.MulterFactory) private multerFactory,
    @inject(TYPES.StorageOptions) private storageOptions,
    @inject(TYPES.PromisifyMulter) private promisifyMulter
  ) {}

  @httpGet('/', passport.authenticate('jwt', { session: false }))
  async getAllUsers(req: Request, res: Response) {
    try {
      const response = await handleGetAllUsersRequest(
        req,
        res,
        this.userService
      );

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpGet('/profile', passport.authenticate('jwt', { session: false }))
  getUserProfile(req: Request, res: Response) {
    const response = handleProfileRequest(req, res);

    res.send(response);
  }

  @httpGet('/confirmEmail/:token')
  async confirmEmail(req: Request, res: Response) {
    try {
      const response = await handleConfirmEmailRequest(
        req,
        res,
        this.userService,
        this.jwt
      );

      res.redirect(response);
    } catch (err) {
      return res.status(400).send('<h1>Confirmation failed...</h1>');
    }
  }

  @httpGet('/byPostId/:postId')
  async getUserByPostId(req: Request, res: Response) {
    try {
      const response = await handelGetUserByPostId(req, res, this.userService);

      res.status(200).send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpGet('/:id', passport.authenticate('jwt', { session: false }))
  async getUserById(req: Request, res: Response) {
    try {
      const response = await handleGetUserById(req, res, this.userService);

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpPost('/register')
  async registerUser(req: Request, res: Response) {
    try {
      const response = await handleRegisterRequest(
        req,
        res,
        this.userService,
        this.nodeMailerWrapper,
        this.jwt
      );

      res.status(201).send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpPost('/authenticate')
  async authenticateUser(req: Request, res: Response) {
    try {
      const response = await handleAuthenticateRequest(
        req,
        res,
        this.userService,
        this.jwt
      );

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpPost(
    '/uploadProfileImageUrl',
    passport.authenticate('jwt', { session: false })
  )
  async uploadProfileImageUrl(req: Request, res: Response) {
    try {
      const response = await handleUploadProfileImageUrlRequest(
        req,
        res,
        this.userService
      );

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpPost('/uploadImage', passport.authenticate('jwt', { session: false }))
  async uploadImage(req: Request, res: Response) {
    try {
      const response = await handleUploadProfileImageRequest(
        req,
        res,
        this.userService,
        this.multerFactory,
        this.storageOptions,
        this.promisifyMulter
      );

      res.send(response);
    } catch (error) {
      handleError(error, res);
    }
  }

  // TODO: This endpoint should be renamed to something more specific.
  // It sends the email to reset the user's password
  @httpPost('/sendEmail')
  async sendEmail(req: Request, res: Response) {
    try {
      const response = await handleSendEmailRequest(
        req,
        res,
        this.userService,
        this.nodeMailerWrapper,
        this.jwt
      );

      res.send({ success: response });
    } catch (err) {
      return res.status(500).send(err);
    }
  }

  // TODO: This endpoint should probably be a PUT request
  @httpPost('/resetPassword', passport.authenticate('jwt', { session: false }))
  async resetPassword(req: Request, res: Response) {
    try {
      const response = await handleResetPasswordRequest(
        req,
        res,
        this.userService
      );

      res.send(response);
    } catch (err) {
      return res.status(500).send(err);
    }
  }
}
