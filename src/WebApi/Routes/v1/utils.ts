
import { Response } from "express";

export type ErrorMessage = {
    message: string;
    stack: string;
}

function isError(error: unknown): error is ErrorMessage {
    return (error as ErrorMessage).message !== undefined;
}

export const handleError = (err: unknown, res: Response<{ errorMessage: string, errorStackTrace: string | null }>) =>
    isError(err) ? res.status(500).send({ errorMessage: err.message, errorStackTrace: err.stack }) : res.send({ errorMessage: 'error', errorStackTrace: null });
