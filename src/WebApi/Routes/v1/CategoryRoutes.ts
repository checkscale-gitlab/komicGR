import passport from 'passport';
import { httpPost, httpGet, controller } from 'inversify-express-utils';
import { Request, Response } from 'express';
import { inject } from 'inversify';

import {
  handleAddCategory,
  handleSetPostCategory,
  handleGetPostCategories
} from '../../Controllers/CategoryController/index';
import TYPES from '../../../Helpers/DI/Types';
import ICategoryService from '../../../Application.Services/Category/ICategoryService';
import IPostService from '../../../Application.Services/Post/IPostService';
import { handleError } from './utils';
import { ROUTES } from '../../../constants';

@controller(`/${ROUTES.API_VERSION.V1}/categories`)
export class CategoryRoute {
  constructor(
    @inject(TYPES.ICategoryService) private categoryService: ICategoryService,
    @inject(TYPES.IPostService) private postService: IPostService
  ) {}

  @httpGet('/')
  async getPostCategories(req: Request, res: Response) {
    try {
      const response = await handleGetPostCategories(
        req,
        res,
        this.categoryService
      );

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpPost('/add', passport.authenticate('jwt', { session: false }))
  async addCategory(req: Request, res: Response) {
    try {
      const response = await handleAddCategory(req, res, this.categoryService);

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpPost('/setToPost', passport.authenticate('jwt', { session: false }))
  async setCategoryToPost(req: Request, res: Response) {
    try {
      const response = await handleSetPostCategory(
        req,
        res,
        this.categoryService,
        this.postService
      );

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }
}
