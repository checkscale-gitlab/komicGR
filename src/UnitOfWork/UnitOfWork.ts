import { injectable, inject } from 'inversify';
import * as mysql from 'promise-mysql';
import { Query, QueryOptions } from 'mysql';

import IUnitOfWork from './IUnitOfWork';
import TYPES from '../Helpers/DI/Types';

type ConnectionProvider = () => Promise<mysql.Pool>;

@injectable()
class UnitOfWork implements IUnitOfWork {
  private connection?: mysql.Pool;
  private transaction?: mysql.PoolConnection;

  constructor(@inject(TYPES.Pool) private poolPromise: ConnectionProvider) {}

  async beginTransaction() {
    this.connection = await this.poolPromise();
    this.transaction = await this.connection.getConnection();

    return this.transaction;
  }

  async query(sql: string | Query | QueryOptions) {
    this.connection = await this.poolPromise();

    return this.connection.query(sql);
  }

  async rollback() {
    this.connection?.release();

    return this.transaction?.rollback();
  }

  async commit(): Promise<void> {
    await this.transaction?.commit();

    return this.release();
  }

  async release() {
    try {
      this.connection?.release();
    } catch (error) {
      console.log(error);
    }
  }

  async escape(input: unknown) {
    return this.connection?.escape(input);
  }
}

export default UnitOfWork;
