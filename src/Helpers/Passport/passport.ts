import { Request } from 'express';
import {
  ExtractJwt,
  Strategy as JwtStrategy,
  StrategyOptions
} from 'passport-jwt';
import { PassportStatic } from 'passport';
import User from '../../Domain/Models/User/User';
import IUserRepository from '../../DataAccess/Sql/Repositories/User/IUserRepository';

export const passportFactory =
  (userRepository: IUserRepository) => (passport: PassportStatic) => {
    const extractJwt = ExtractJwt;
    const options: StrategyOptions = {
      jwtFromRequest: extractJwt.fromAuthHeaderWithScheme('jwt'),
      secretOrKey: process.env.secret,
      passReqToCallback: true
    };

    passport.use(
      new JwtStrategy(options, (req: Request, jwt_payload, done) => {
        if (jwt_payload.email) {
          userRepository
            .getByEmail(jwt_payload.email)
            .then((user: User) => {
              if (user) {
                req.userFromPassport = { ...user };

                return done(null, User.toProfileResponse(user));
              } else {
                return done(new Error('User not found'), null);
              }
            })
            .catch((err) => {
              return done(err, null);
            });
        } else {
          userRepository
            .getById(jwt_payload.Id)
            .then((user: User) => {
              if (user) {
                req.userFromPassport = { ...user };

                return done(null, User.toProfileResponse(user));
              } else {
                return done(new Error('User not found'), null);
              }
            })
            .catch((err) => {
              return done(err, null);
            });
        }
      })
    );
  };
