import { RequestHandler } from "express-serve-static-core";
import { Request, Response } from 'express';

export function promisifyMulter(multerInstance: RequestHandler, req: Request, res: Response) {
    return new Promise<void>((resolve, reject) => {
        multerInstance(req, res, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}
