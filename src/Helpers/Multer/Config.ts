import { Request } from "express";
import multer from "multer";

export const storageOptions: multer.DiskStorageOptions = {
    destination: 'uploads/profile-images/',
    filename: function (req: Request, file: { originalname: string }, callback: Function) {
        callback(null, 'profile.' + file.originalname.split('.')[1]);
    }
};

export const multerFactory = (options: multer.DiskStorageOptions) => (): multer.Multer => multer({ limits: { fileSize: 200000 }, storage: multer.diskStorage(options) });
