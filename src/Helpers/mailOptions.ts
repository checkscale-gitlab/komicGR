export const getMailOptions = () => ({
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    secure: process.env.MAIL_SECURE,
    auth: {
        user: process.env.MAIL_AUTH_USER,
        pass: process.env.MAIL_AUTH_PASS
    },
    tls: {
        rejectUnauthorized: process.env.MAIL_TLS_REJECTUNAUTHORIZED
    }
});