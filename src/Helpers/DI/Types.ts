const TYPES = {
  Pool: Symbol.for("Pool"),
  IUnitOfWork: Symbol.for("IUnitOfWork"),
  ICategoryService: Symbol.for("ICategoryService"),
  ICategoryRepository: Symbol.for("ICategoryRepository"),
  IPostService: Symbol.for("IPostService"),
  IPostRepository: Symbol.for("IPostRepository"),
  IUserService: Symbol.for("IUserService"),
  IUserRepository: Symbol.for("IUserRepository"),
  ITagService: Symbol.for("ITagService"),
  ITagRepository: Symbol.for("ITagRepository"),
  INodeMailerWrapper: Symbol.for("INodeMailerWrapper"),
  JWT: Symbol.for("JWT"),
  MulterFactory: Symbol.for("MulterFactory"),
  StorageOptions: Symbol.for("StorageOptions"),
  PromisifyMulter: Symbol.for("PromisifyMulter"),
  BcryptService: Symbol.for("BcryptService"),
};

export default TYPES;
