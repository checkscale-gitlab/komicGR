import { Container } from 'inversify';
import * as jwt from 'jsonwebtoken';
import { multerFactory, storageOptions } from '../Multer/Config';
import * as mysql from 'promise-mysql';

import TYPES from './Types';

import IUnitOfWork from '../../UnitOfWork/IUnitOfWork';
import UnitOfWork from '../../UnitOfWork/UnitOfWork';
import ICategoryService from '../../Application.Services/Category/ICategoryService';
import CategoryService from '../../Application.Services/Category/CategoryService';
import ICategoryRepository from '../../DataAccess/Sql/Repositories/Category/ICategoryRepository';
import CategoryRepository from '../../DataAccess/Sql/Repositories/Category/CategoryRepository';
import PostService from '../../Application.Services/Post/PostService';
import IPostService from '../../Application.Services/Post/IPostService';
import PostRepository from '../../DataAccess/Sql/Repositories/Post/PostRepository';
import IPostRepository from '../../DataAccess/Sql/Repositories/Post/IPostRepository';
import NodeMailerWrapper from '../../Helpers/NodeMailer/NodeMailerWrapper';
import INodeMailerWrapper from '../../Helpers/NodeMailer/INodeMailerWrapper';
import { getMailOptions } from '../mailOptions';
import IUserService from '../../Application.Services/User/IUserService';
import UserService from '../../Application.Services/User/UserService';
import IUserRepository from '../../DataAccess/Sql/Repositories/User/IUserRepository';
import UserRepository from '../../DataAccess/Sql/Repositories/User/UserRepository';
import ITagService from '../../Application.Services/Tag/ITagService';
import TagService from '../../Application.Services/Tag/TagService';
import ITagRepository from '../../DataAccess/Sql/Repositories/Tag/ITagRepository';
import TagRepository from '../../DataAccess/Sql/Repositories/Tag/TagRepository';
import { promisifyMulter } from '../../Helpers/Multer/PromisifyMulter';
import BcryptService from '../../Helpers/Bcryptjs/BcryptService';
import UnitOfWorkFactory from '../../UnitOfWork/UnitOfWorkFactory';

// pool.on('acquire', function (connection) {
//     console.log('Connection %d acquired', connection.threadId);
// });

// pool.on('connection', function (connection) {
//     connection.query('SET SESSION auto_increment_increment=1')
// });

// pool.on('release', (connection) => {
//     console.log('Connection %d released', connection.threadId);
// });

export function CreateContainer(): Container {
  const container = new Container();

  container
    .bind<Promise<mysql.Pool>>(TYPES.Pool)
    .toProvider(() => async () => await UnitOfWorkFactory.createPool());
  container.bind<IUnitOfWork>(TYPES.IUnitOfWork).to(UnitOfWork);
  container.bind<ICategoryService>(TYPES.ICategoryService).to(CategoryService);
  container
    .bind<ICategoryRepository>(TYPES.ICategoryRepository)
    .to(CategoryRepository);
  container.bind<IPostService>(TYPES.IPostService).to(PostService);
  container.bind<IPostRepository>(TYPES.IPostRepository).to(PostRepository);
  container.bind<IUserService>(TYPES.IUserService).to(UserService);
  container.bind<IUserRepository>(TYPES.IUserRepository).to(UserRepository);
  container.bind<ITagService>(TYPES.ITagService).to(TagService);
  container.bind<ITagRepository>(TYPES.ITagRepository).to(TagRepository);
  container
    .bind<INodeMailerWrapper>(TYPES.INodeMailerWrapper)
    .toConstantValue(new NodeMailerWrapper(getMailOptions()));
  // TODO: Fix this types
  container.bind<any>(TYPES.JWT).toConstantValue(jwt);
  container.bind<any>(TYPES.MulterFactory).toConstantValue(multerFactory);
  container.bind<any>(TYPES.StorageOptions).toConstantValue(storageOptions);
  container.bind<any>(TYPES.PromisifyMulter).toConstantValue(promisifyMulter);
  container
    .bind<BcryptService>(TYPES.BcryptService)
    .toConstantValue(BcryptService);

  return container;
}
