import { Request, Response } from "express";
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { handleSetPostCategory } from '../../../WebApi/Controllers/CategoryController/index';

import ICategoryService from "../../../Application.Services/Category/ICategoryService";
import { CategoryServiceMOCK } from "../../Mocks/CategoryServiceMock";
import { PostServiceMOCK } from "../../Mocks/PostServiceMock";
import SetCategoryDTO from "../../../Domain/Dtos/Category/SetCategoryDTO";
import Post from "../../../Domain/Models/Post/Post";
import User from "../../../Domain/Models/User/User";
import Role from "../../../Domain/Models/Role/Role";
import { createTestPost } from "../../Helpers/postUtils";

describe('SetPostCategoryHandler', () => {
    let request: Partial<Request>;
    let response: Response;
    let categoryService: ICategoryService;

    beforeEach(() => {
        request = {
            body: {
                username: 'username',
                password: 'password'
            },
            params: {},
            userFromPassport: {} as User
        }
        categoryService = new CategoryServiceMOCK();
    });

    it('should return an object with success property set to true and msg property set to "Post Category Set" when setting the post\'s category', async () => {
        const setCategoryDTO: SetCategoryDTO = {
            Post_id: 1,
            Category_id: 1
        }
        const dummyPost = createTestPost();
        const postService = new PostServiceMOCK()
        const stubSetPostCategory = sinon.stub(categoryService, "setPostCategory").callsFake((postId: number, categoryId: number) => Promise.resolve(true));
        const stubGetPostByPostId = sinon.stub(postService, "getPostByPostId").callsFake((postId: number) => Promise.resolve(dummyPost));
        request.body = setCategoryDTO;
        request.userFromPassport!.id = dummyPost.id;
        const handler = handleSetPostCategory(request as Request, response, categoryService, postService)
        const setCategory = await handler;

        stubSetPostCategory.restore();
        stubGetPostByPostId.restore();
        expect(stubSetPostCategory.calledOnceWith(setCategoryDTO.Post_id, setCategoryDTO.Category_id)).to.be.true;
        expect(stubGetPostByPostId.calledOnceWith(setCategoryDTO.Post_id)).to.be.true;
        expect(handler).to.be.instanceOf(Promise);
        expect(setCategory.success).to.equal(true);
        expect(setCategory.msg).to.deep.equal('Post Category Set');
    });

    it('should throw an error with the message "You are not allowed to set this post\'s category" when a user tries to set another user\'s post category', async () => {
        const setCategoryDTO: SetCategoryDTO = {
            Post_id: 1,
            Category_id: 1
        }
        const dummyPost = createTestPost();
        const anotherUserId = 2
        const postService = new PostServiceMOCK()
        const stubSetPostCategory = sinon.stub(categoryService, "setPostCategory").callsFake((postId: number, categoryId: number) => Promise.resolve(true));
        const stubGetPostByPostId = sinon.stub(postService, "getPostByPostId").callsFake((postId: number) => Promise.resolve(dummyPost));
        request.body = setCategoryDTO;
        request.userFromPassport!.id = anotherUserId;

        try {
            await handleSetPostCategory(request as Request, response, categoryService, postService);
            expect.fail();
        } catch (error) {
            expect(error).to.deep.equal({ message: 'You are not allowed to set this post\'s category' });
            expect(stubSetPostCategory.calledOnceWith(setCategoryDTO.Post_id, setCategoryDTO.Category_id)).to.be.false;
            expect(stubGetPostByPostId.calledOnceWith(setCategoryDTO.Post_id)).to.be.true;
        }

        stubSetPostCategory.restore();
        stubGetPostByPostId.restore();
    });

    it('should throw an error with the message "You are not authorized for this action", when a user with the role Human tries to set a category', async () => {
        const setCategoryDTO: SetCategoryDTO = {
            Post_id: 1,
            Category_id: 1
        }
        const dummyPost = createTestPost(setCategoryDTO.Post_id, 1, [], setCategoryDTO.Category_id);
        const postService = new PostServiceMOCK()
        const stubGetPostByPostId = sinon.stub(postService, "getPostByPostId").resolves(dummyPost);
        request.body = setCategoryDTO;
        request.userFromPassport!.id = dummyPost.user_id;
        request.userFromPassport!.role = Role.Human;

        try {
            await handleSetPostCategory(request as Request, response, categoryService, postService);
            expect.fail();
        } catch (error) {
            expect(error).to.deep.equal({ message: 'You are not authorized for this action' });
            expect(stubGetPostByPostId.calledOnceWith(setCategoryDTO.Post_id)).to.be.true;
        }

        stubGetPostByPostId.restore();
    });
});