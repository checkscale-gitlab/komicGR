import { Request, Response } from "express";
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { handleAddCategory } from '../../../WebApi/Controllers/CategoryController/index';
import AddCategoryDTO from "../../../Domain/Dtos/Category/AddCategoryDTO";
import ICategoryService from "../../../Application.Services/Category/ICategoryService";
import { CategoryServiceMOCK } from "../../Mocks/CategoryServiceMock";
import User from "../../../Domain/Models/User/User";
import Role from "../../../Domain/Models/Role/Role";

describe('AddCategoryHandler', () => {
    let request: Partial<Request>;
    let response: Response;
    let categoryService: ICategoryService;

    beforeEach(() => {
        const user = new User(
            1,
            Role.SuperHuman,
            "Test First Name",
            "Test Last Name",
            "test@test.com",
            "test_username",
            "test_pass",
            "test job_desc",
            "test address",
            11111111,
            "http://www.someurl.com",
            new Date()
        )
        request = {
            userFromPassport: user,
            params: {}
        }
        categoryService = new CategoryServiceMOCK();
    });

    it('should return a promise with an object with property success set to true and msg "Category added" if the user has the appropriate role', async () => {
        const categoryToAdd: AddCategoryDTO = {
            Name: 'Test category',
            Description: 'Test description'
        };
        const stubAddCategory = sinon.stub(categoryService, "addCategory").callsFake(() => Promise.resolve());
        const handler = handleAddCategory(request as Request, response, categoryService)
        const added = await handler;

        stubAddCategory.restore()
        expect(handler).to.be.instanceOf(Promise);
        expect(added).to.deep.equal({ success: true, msg: "Category added" });
    });

    it('should throw an error with the message "You are not authorized for this action" when the user has the role of human', async () => {
        const categoryToAdd: AddCategoryDTO = {
            Name: 'Test category',
            Description: 'Test description'
        };
        request.userFromPassport!.role = Role.Human;
        const stubAddCategory = sinon.stub(categoryService, "addCategory").callsFake(() => Promise.resolve());

        try {
            await handleAddCategory(request as Request, response, categoryService)
            expect.fail('Expected to fail with Error (You are not authorized for this action)')
        } catch (error) {
            expect(error).to.deep.equal({ message: "You are not authorized for this action" });
        }

        stubAddCategory.restore()
    });
})