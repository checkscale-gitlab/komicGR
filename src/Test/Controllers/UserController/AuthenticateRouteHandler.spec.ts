import { Request, Response } from 'express';
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { handleAuthenticateRequest } from '../../../WebApi/Controllers/UserController/index';
import IUserService from '../../../Application.Services/User/IUserService';
import { UserServiceMOCK } from '../../Mocks/UserServiceMock';

import User from '../../../Domain/Models/User/User';
import Role from '../../../Domain/Models/Role/Role';

function createTestUser(): User {
    return new User(
        1,
        Role.Human,
        "Test User First Name",
        "Test User Last Name",
        "test@test.com",
        "TestUsername",
        "Test Password",
        "Test Job description",
        "Test address",
        1234567890,
        "http://www.someurl.com",
        new Date(),
        true
    )
}

describe('AuthenticateRouteHandler', () => {
    let request: Partial<Request>;
    let response: Response;
    let userService: IUserService;
    const success = {
        success: true,
        token: 'demo token'
    };
    const jwt = {
        sign: (obj1: any, secret: string, obj2: any) => 'demo token'
    }
    const testUser = createTestUser();

    beforeEach(() => {
        request = {
            body: {
                username: 'username',
                password: 'password'
            },
            params: {}
        };
        userService = new UserServiceMOCK();
        request.body.username = testUser.username;
        request.body.password = testUser.password;
    });

    describe('success cases', () => {
        it('should return an object with success property true and a token property with the jwt token', async () => {
            const stubGetByUsername = sinon.stub(userService, "getByUsername").callsFake(() => {
                return Promise.resolve(testUser)
            });
            const stubComparePassword = sinon.stub(userService, "comparePassword").callsFake(() => {
                return Promise.resolve(true)
            });

            request.body.username = testUser.username;
            request.body.password = testUser.password;

            const handler = handleAuthenticateRequest(request as Request, response, userService, jwt);
            const user = await handler;

            stubGetByUsername.restore();
            stubComparePassword.restore();

            expect(handler).to.be.instanceOf(Promise);
            expect(stubGetByUsername.calledOnceWith(testUser.username)).to.be.true;
            expect(stubComparePassword.calledOnceWith(request.body.password, testUser.password)).to.be.true;
            expect(user).to.deep.equal(success);
        });
    });
    describe("error cases", () => {
        it('should throw an error with the message \'User doesn\'t exist\' if the user is not found by the username', async () => {
            const stubGetByUsername = sinon.stub(userService, "getByUsername").callsFake(() => {
                return Promise.resolve(null);
            });

            try {
                await handleAuthenticateRequest(request as Request, response, userService, jwt);
                expect.fail("Expected to throw Error (User doesn't exist")
            } catch (error) {
                expect(error).to.deep.equal({ message: "User doesn't exist" })
                expect(stubGetByUsername.calledOnceWith(testUser.username)).to.be.true;
            }

            stubGetByUsername.restore();
        });

        it('should throw an error with the message \'Wrong Credentials\', if password don\'t match with the hash', async () => {
            const stubGetByUsername = sinon.stub(userService, "getByUsername").callsFake(() => {
                return Promise.resolve(testUser)
            });
            const stubComparePassword = sinon.stub(userService, "comparePassword").callsFake(() => {
                return Promise.resolve(false)
            });

            try {
                await handleAuthenticateRequest(request as Request, response, userService, jwt)
                expect.fail('Expected to throw Error (Wrong Credentials)')
            } catch (error) {
                expect(error).to.deep.equal({ message: "Wrong Credentials" });
                expect(stubGetByUsername.calledOnceWith(testUser.username)).to.be.true;
                expect(stubComparePassword.calledOnceWith(request.body.password, testUser.password)).to.be.true;
            }

            stubGetByUsername.restore();
            stubComparePassword.restore();

        });

        it('should throw an error with the message \'Please confirm your email\' if user exists but the email is not confirmed', async () => {
            const stubGetByUsername = sinon.stub(userService, "getByUsername").callsFake(() => {
                return Promise.resolve(testUser)
            });
            testUser.confirmed = false;

            try {
                await handleAuthenticateRequest(request as Request, response, userService, jwt);
                expect.fail('Expected to throw Error (Please confirm your email)')
            } catch (error) {
                expect(error).to.deep.equal({ message: "Please confirm your email" })
                expect(stubGetByUsername.calledOnceWith(testUser.username)).to.be.true;
            }

            stubGetByUsername.restore();
        });
    });
});