// import { Request, Response } from 'express';
// import sinon from 'sinon';
// import chai from 'chai';
// const expect = chai.expect;
// import multer from 'multer';

// import { handleUploadProfileImageRequest } from '../../../WebApi/Controllers/UserController/index';

// import IUserService from '../../../Application.Services/User/IUserService';
// import { UserServiceMOCK } from '../../Mocks/UserServiceMock';
// import User from '../../../Domain/Models/User/User';

// describe('UploadProfileImageRouteHandler', () => {
//     let request: Partial<Request>;
//     let response: Partial<Response>;
//     let userService: IUserService;

//     const storageOptions: multer.DiskStorageOptions = {
//         destination: 'uploads/',
//         filename: function (req: Request, file: { originalname: string }, callback: Function) {
//             callback(null, 'profile.' + file.originalname.split('.')[1]);
//         }
//     };
//     const singleFunction = (userFile: string) => 'SingleFunctionMock';
//     const multerStub = sinon.stub(multer, "diskStorage").callsFake(() => { return { single: singleFunction } });
//     const multerFactory = (options: multer.DiskStorageOptions) => (() => multerStub({ storage: options }));

//     beforeEach(() => {
//         request = {
//             userFromPassport: {
//                 id: 1
//             } as User,
//             file: {
//                 originalname: 'test.png'
//             } as Express.Multer.File
//         };
//         userService = new UserServiceMOCK();
//         multerStub.callCount = 0;
//     });

//     afterEach(() => {
//         multerStub.restore();
//     });

//     describe('success cases', () => {
//         it('should return an object with success property true and msg "Profile Image added succesfully"', async () => {
//             const storageOptionsClone = Object.assign({}, storageOptions);
//             storageOptionsClone.destination = (storageOptions.destination! as String).concat(String(request.userFromPassport!.id)).concat('/');
//             const profileImageUrl = '/profile-images/' + request.userFromPassport!.id + '/profile.' + request.file!.originalname.split('.')[1];
//             const stubSetProfileImageUrl = sinon.stub(userService, "setProfileImageUrl").callsFake(() => Promise.resolve(true));
//             const spyMulterFactory = sinon.spy(multerFactory);
//             const promisifyMulterMock: Function = () => Promise.resolve();
//             // const spyPromisifyMulterMock = sinon.spy(promisifyMulterMock).withArgs(singleFunction, request, response);
//             const handler = handleUploadProfileImageRequest(request as Request, response as Response, userService, spyMulterFactory, storageOptions, promisifyMulterMock);
//             const updated = await handler;

//             stubSetProfileImageUrl.restore();
//             expect(handler).to.be.instanceOf(Promise);
//             expect(spyMulterFactory.calledOnce).to.be.true;
//             expect(spyMulterFactory.getCall(0).args[0]).to.deep.equal(storageOptionsClone);
//             // expect(spyPromisifyMulterMock.calledOnceWith(singleFunction('userFile'), request, response)).to.be.true;
//             expect(multerStub.calledOnce).to.be.true;
//             expect(multerStub.getCall(0).args[0]).to.deep.equal({ storage: storageOptionsClone });
//             expect(stubSetProfileImageUrl.calledOnceWithExactly(request.userFromPassport!.id, profileImageUrl)).to.be.true;
//             expect(updated).to.deep.equal({ success: true, msg: "Profile Image added succesfully" });
//         });
//         it('should return an object with success property false and msg "Profile Image added succesfully"', async () => {
//             const storageOptionsClone = Object.assign({}, storageOptions);
//             storageOptionsClone.destination = (storageOptions.destination! as String).concat(String(request.userFromPassport!.id)).concat('/');
//             const profileImageUrl = '/profile-images/' + request.userFromPassport!.id + '/profile.' + request.file!.originalname.split('.')[1];
//             const stubSetProfileImageUrl = sinon.stub(userService, "setProfileImageUrl").callsFake(() => Promise.resolve(false));
//             const spyMulterFactory = sinon.spy(multerFactory);
//             const promisifyMulterMock: Function = () => Promise.resolve();
//             const handler = handleUploadProfileImageRequest(request as Request, response as Response, userService, spyMulterFactory, storageOptions, promisifyMulterMock);
//             const updated = await handler;

//             stubSetProfileImageUrl.restore();
//             expect(handler).to.be.instanceOf(Promise);
//             expect(spyMulterFactory.calledOnce).to.be.true;
//             expect(spyMulterFactory.getCall(0).args[0]).to.deep.equal(storageOptionsClone);
//             expect(multerStub.calledOnce).to.be.true;
//             expect(multerStub.getCall(0).args[0]).to.deep.equal({ storage: storageOptionsClone });
//             expect(stubSetProfileImageUrl.calledOnceWithExactly(request.userFromPassport!.id, profileImageUrl)).to.be.true;
//             expect(updated).to.deep.equal({ success: false, msg: "Profile Image added succesfully" });
//         });
//     });
//     describe('error cases', () => {
//         it('should return an error with the errorMessage and the errorStackTrace properties, if the promisifyMulter throws an error', async () => {
//             const storageOptionsClone = Object.assign({}, storageOptions);
//             storageOptionsClone.destination = (storageOptions.destination! as String).concat(String(request.userFromPassport!.id)).concat('/');
//             const profileImageUrl = '/profile-images/' + request.userFromPassport!.id + '/profile.' + request.file!.originalname.split('.')[1];
//             const stubSetProfileImageUrl = sinon.stub(userService, "setProfileImageUrl").callsFake(() => Promise.resolve(true));
//             const spyMulterFactory = sinon.spy(multerFactory);
//             const error = { message: 'ErrorMessage', stack: "ErrorStackTrace" };
//             const promisifyMulterMock: Function = () => Promise.reject(error);
//             const handler = handleUploadProfileImageRequest(request as Request, response as Response, userService, spyMulterFactory, storageOptions, promisifyMulterMock);
//             const updated = await handler;

//             stubSetProfileImageUrl.restore();

//             expect(handler).to.be.instanceOf(Promise);
//             expect(spyMulterFactory.calledOnce).to.be.true;
//             expect(spyMulterFactory.getCall(0).args[0]).to.deep.equal(storageOptionsClone);
//             expect(multerStub.calledOnce).to.be.true;
//             expect(multerStub.getCall(0).args[0]).to.deep.equal({ storage: storageOptionsClone });
//             expect(stubSetProfileImageUrl.calledOnce).to.be.false;
//             expect(updated).to.deep.equal({ errorMessage: error.message, errorStackTrace: error.stack });
//         });
//         it('should return an error with the errorMessage and the errorStackTrace properties, if the setProfileImageUrl throws an error', async () => {
//             const storageOptionsClone = Object.assign({}, storageOptions);
//             storageOptionsClone.destination = (storageOptions.destination! as String).concat(String(request.userFromPassport!.id)).concat('/');
//             const profileImageUrl = '/profile-images/' + request.userFromPassport!.id + '/profile.' + request.file!.originalname.split('.')[1];
//             const error = { message: 'ErrorMessage', stack: "ErrorStackTrace" };
//             const stubSetProfileImageUrl = sinon.stub(userService, "setProfileImageUrl").callsFake(() => Promise.reject(error));
//             const spyMulterFactory = sinon.spy(multerFactory);
//             const promisifyMulterMock: Function = () => Promise.resolve();
//             const handler = handleUploadProfileImageRequest(request as Request, response as Response, userService, spyMulterFactory, storageOptions, promisifyMulterMock);
//             const updated = await handler;

//             stubSetProfileImageUrl.restore();

//             expect(handler).to.be.instanceOf(Promise);
//             expect(spyMulterFactory.calledOnce).to.be.true;
//             expect(spyMulterFactory.getCall(0).args[0]).to.deep.equal(storageOptionsClone);
//             expect(multerStub.calledOnce).to.be.true;
//             expect(multerStub.getCall(0).args[0]).to.deep.equal({ storage: storageOptionsClone });
//             expect(stubSetProfileImageUrl.calledOnce).to.be.true;
//             expect(updated).to.deep.equal({ errorMessage: error.message, errorStackTrace: error.stack });
//         });

//     });
// });