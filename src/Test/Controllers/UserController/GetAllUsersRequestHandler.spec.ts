import { Request, Response } from "express";
import sinon from "sinon";
import chai from "chai";
const expect = chai.expect;

import { handleGetAllUsersRequest } from "../../../WebApi/Controllers/UserController/index";
import IUserService from "../../../Application.Services/User/IUserService";
import { UserServiceMOCK } from "../../Mocks/UserServiceMock";

import User from "../../../Domain/Models/User/User";
import Role from "../../../Domain/Models/Role/Role";

function createTestUserWithId(id: number): User {
  return new User(
    id,
    Role.Human,
    "Test First Name",
    "Test Last Name",
    "test" + id + "@test.com",
    "test_username_" + id,
    "test_pass",
    "test job_desc",
    "test address",
    11111111,
    "http://www.someurl.com",
    new Date()
  );
}

describe("GetAllUsersRequestHandler", () => {
  let request: Request;
  let response: Response;
  let userService: IUserService;
  beforeEach(() => {
    userService = new UserServiceMOCK();
  });

  it("should return a promise with an array with all the user profiles response object", async () => {
    const testUser1 = createTestUserWithId(1);
    const testUser2 = createTestUserWithId(2);
    const testUsers = [testUser1, testUser2];
    const stubGetAll = sinon
      .stub(userService, "getAll")
      .callsFake(() => Promise.resolve(testUsers));
    const handler = handleGetAllUsersRequest(request, response, userService);
    const users = await handler;

    stubGetAll.restore();
    expect(handler).to.be.instanceOf(Promise);
    expect(users).to.deep.equal(User.toProfileResponseArray(testUsers));
  });
});
