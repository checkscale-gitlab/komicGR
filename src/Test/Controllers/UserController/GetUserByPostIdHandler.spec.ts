import { Request, Response } from 'express';
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { handelGetUserByPostId } from '../../../WebApi/Controllers/UserController/index';
import IUserService from '../../../Application.Services/User/IUserService';
import { UserServiceMOCK } from '../../Mocks/UserServiceMock';

import User from '../../../Domain/Models/User/User';
import Role from '../../../Domain/Models/Role/Role';

function createTestUser(): User {
    return new User(
        1,
        Role.Human,
        "first_name",
        "last_name",
        "test@test.com",
        "test username",
        "test pass",
        "test job_desc",
        "test address",
        111111111,
        "http://www.someurl.com",
        new Date()
    )
}

describe('GetUserByPostIdRequestHandler', () => {
    let request: Partial<Request> = {};
    let response: Response;
    let userService: IUserService;

    beforeEach(() => {
        request = {
            params: {
                postId: '1'
            }
        }
        userService = new UserServiceMOCK();
    });

    it('should return an object with success property set to true and user property set to the user profile', async () => {
        const testUser = createTestUser();
        const userProfileResponse1 = User.toProfileResponse(testUser);
        const stubGetByPostId = sinon.stub(userService, "getUserByPostId").callsFake(() => Promise.resolve(testUser));
        const handler = handelGetUserByPostId(request as Request, response as Response, userService);
        const userProfileResponse = await handler;

        stubGetByPostId.restore();
        expect(handler).to.be.instanceOf(Promise);
        expect(userProfileResponse).to.deep.equal({ success: true, user: userProfileResponse1 });
    });
});