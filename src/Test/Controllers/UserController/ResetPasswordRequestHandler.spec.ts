import { Request, Response } from 'express';
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import IUserService from "../../../Application.Services/User/IUserService";
import { UserServiceMOCK } from "../../Mocks/UserServiceMock";
import User from "../../../Domain/Models/User/User";
import Role from '../../../Domain/Models/Role/Role';
import { handleResetPasswordRequest } from '../../../WebApi/Controllers/UserController/index';

function createTestUser(): User {
    return new User(
        1,
        Role.Human,
        "first_name",
        "last_name",
        "test@test.com",
        "test username",
        "test pass",
        "test job_desc",
        "test address",
        111111111,
        "http://www.someurl.com",
        new Date()
    )
}

describe('ResetPasswordRequestHandler', () => {
    let request: Partial<Request> = {};
    let response: Response;
    let userService: IUserService;
    const testUser = createTestUser();

    beforeEach(() => {
        request = {
            body: {},
            userFromPassport: testUser
        }
        userService = new UserServiceMOCK();
    });

    describe('success cases', () => {
        it('should return an object with success property set to true', async () => {
            request.body.newPassword = 'newPassword';
            const stubUservice = sinon.stub(userService, "updatePassword").returns(Promise.resolve(true))
            const handler = handleResetPasswordRequest(request as Request, response, userService);
            const resetResponse = await handler;

            stubUservice.restore();

            expect(resetResponse).to.deep.equal({ success: true });
            expect(stubUservice.calledOnceWith(testUser.id, request.body.newPassword)).to.be.true;
        });
    });

    describe('error cases', () => {
        it('should return an object with success property set to false if the user service returns false', async () => {
            request.body.newPassword = 'newPassword';
            const stubUservice = sinon.stub(userService, "updatePassword").returns(Promise.resolve(false))
            const handler = handleResetPasswordRequest(request as Request, response, userService);
            const resetResponse = await handler;

            stubUservice.restore();

            expect(resetResponse).to.deep.equal({ success: false });
            expect(stubUservice.calledOnceWith(testUser.id, request.body.newPassword)).to.be.true;
        });
        it('should return an object with success property set to false if the new password is less than 8 characters', async () => {
            request.body.newPassword = 'new';
            const stubUservice = sinon.stub(userService, "updatePassword").returns(Promise.resolve(false))
            const handler = handleResetPasswordRequest(request as Request, response, userService);

            try {
                await handler;
            } catch (error) {
                stubUservice.restore();

                expect(error).to.be.instanceOf(Error);
                expect(error.message).to.equal('A password must have at least 8 characters');
                expect(stubUservice.calledOnceWith(testUser.id, request.body.newPassword)).to.be.false;

            }
        });
    });
});