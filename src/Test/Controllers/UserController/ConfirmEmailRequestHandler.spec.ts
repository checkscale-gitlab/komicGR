import { Request, Response } from "express";
import sinon from "sinon";
import chai from "chai";
const expect = chai.expect;

import { handleConfirmEmailRequest } from "../../../WebApi/Controllers/UserController/index";
import IUserService from "../../../Application.Services/User/IUserService";
import { UserServiceMOCK } from "../../Mocks/UserServiceMock";

describe("ConfirmEmailRequestHandler", () => {
  let request: Partial<Request>;
  let response: Partial<Response>;
  let userService: IUserService;
  const token = "test token";
  const jwt = {
    verify: () => {
      return { Id: 1 };
    },
  };
  beforeEach(() => {
    request = {
      params: {
        token,
      },
    };
    userService = new UserServiceMOCK();
    process.env.APPLICATION_URL = 'demo_url';
    process.env.APPLICATION_ERROR_PAGE = 'demo_error_page';
  });

  afterEach(() => {
    delete process.env.APPLICATION_URL;
    delete process.env.APPLICATION_ERROR_PAGE;
  })

  it("should return a promise with a value of " + process.env.APPLICATION_URL ?? '' + ", if the email is successfuly confirmed", async () => {
    const stubConfirmEmail = sinon
      .stub(userService, "setConfirmedEmail")
      .callsFake(() => Promise.resolve(true));
    const spyVerify = sinon.spy(jwt, "verify");
    const handler = handleConfirmEmailRequest(
      request as Request,
      response as Response,
      userService,
      jwt
    );
    const url = await handler;

    stubConfirmEmail.restore();
    spyVerify.restore();
    expect(handler).to.be.instanceOf(Promise);
    expect(stubConfirmEmail.calledOnceWith(jwt.verify()["Id"])).to.be.true;
    expect(spyVerify.calledOnceWith(token, process.env.confirmEmailSecret)).to
      .be.true;
    expect(url).to.equal(process.env.APPLICATION_URL);
  });
  it("should return a promise with a value of " + process.env.APPLICATION_ERROR_PAGE ?? '' + ", if the email is not confirmed", async () => {
    const stubConfirmEmail = sinon
      .stub(userService, "setConfirmedEmail")
      .callsFake(() => Promise.resolve(false));
    const spyVerify = sinon.spy(jwt, "verify");
    const handler = handleConfirmEmailRequest(
      request as Request,
      response as Response,
      userService,
      jwt
    );
    const url = await handler;

    stubConfirmEmail.restore();
    spyVerify.restore();
    expect(handler).to.be.instanceOf(Promise);
    expect(stubConfirmEmail.calledOnceWith(jwt.verify()["Id"])).to.be.true;
    expect(spyVerify.calledOnceWith(token, process.env.confirmEmailSecret)).to
      .be.true;
    expect(url).to.equal(process.env.APPLICATION_ERROR_PAGE);
  });
});
