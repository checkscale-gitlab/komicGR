import { Request, Response } from 'express';
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { handleGetUserById } from '../../../WebApi/Controllers/UserController/index';
import IUserService from '../../../Application.Services/User/IUserService';
import { UserServiceMOCK } from '../../Mocks/UserServiceMock';

import User from '../../../Domain/Models/User/User';
import Role from '../../../Domain/Models/Role/Role';

function createTestUserWithId(id: number): User {
    return new User(
        id,
        Role.Human,
        "Test First Name",
        "Test Last Name",
        "test" + id + "@test.com",
        "test_username_" + id,
        "test_pass",
        "test job_desc",
        "test address",
        11111111,
        "http://www.someurl.com",
        new Date()
    )
}

describe('GetUserByIdRequestHandler', () => {
    let request: Partial<Request> = {};
    let response: Response;
    let userService: IUserService;
    beforeEach(() => {
        request = {
            params: {
                id: '1'
            }
        }
        userService = new UserServiceMOCK();
    });

    it('should return a promise with an array with all the user profiles response object', async () => {
        const testUser1 = createTestUserWithId(1);
        const stubGetById = sinon.stub(userService, "getById").callsFake(() => Promise.resolve(testUser1));
        const handler = handleGetUserById(request as Request, response, userService);
        const user = await handler;

        stubGetById.restore();
        expect(handler).to.be.instanceOf(Promise);
        expect(user).to.deep.equal(User.toProfileResponse(testUser1));
    });
});