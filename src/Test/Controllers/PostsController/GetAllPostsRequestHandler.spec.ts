import { Request, Response } from "express";
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { handleGetAllPosts } from '../../../WebApi/Controllers/PostsController/index';
import IPostService from "../../../Application.Services/Post/IPostService";
import { PostServiceMOCK } from "../../Mocks/PostServiceMock";
import Post from "../../../Domain/Models/Post/Post";
import { createTestPost } from "../../Helpers/postUtils";

describe('GetAllPostsRequestHandler', () => {
    let request: Request;
    let response: Response;
    let postService: IPostService;

    beforeEach(() => {
        postService = new PostServiceMOCK();
    });

    it('should return an object with a posts property and as value the post array response', async () => {
        const dummyPosts = [
            createTestPost(1, 1, [], 1),
            createTestPost(2, 1, [], 2)
        ];
        const stubGetAllPosts = sinon.stub(postService, "getAllPosts").callsFake(() => Promise.resolve(dummyPosts));
        const handler = handleGetAllPosts(request, response, postService);
        const result = await handler;

        stubGetAllPosts.restore();
        expect(handler).to.be.instanceOf(Promise);
        expect(result.posts.length).equal(2);
        expect(result.posts).deep.equal(Post.toPostArrayResponse(dummyPosts));
    });
});