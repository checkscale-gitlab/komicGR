import { Request, Response } from "express";
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { handleGetPostLikeStatusRequest } from '../../../WebApi/Controllers/PostsController/index';
import IPostService from "../../../Application.Services/Post/IPostService";
import { PostServiceMOCK } from "../../Mocks/PostServiceMock";
import Post from "../../../Domain/Models/Post/Post";
import User from "../../../Domain/Models/User/User";
import { createTestPost } from "../../Helpers/postUtils";

describe('GetPostUserLikeStatus', () => {
    let request: Partial<Request>;
    let response: Response;
    let postService: IPostService;

    beforeEach(() => {
        request = {
            params: {},
            userFromPassport: {} as User
        }
        postService = new PostServiceMOCK();
    });

    it('should return an object with like property set to true when requesting a user\'s post that he\/she already liked', async () => {
        const userId = 1;
        const dummyPost = createTestPost();
        request.userFromPassport!.id = dummyPost.user_id;
        request.params!.postId = String(dummyPost.id);
        const stubAGetPostUserLikeStatus = sinon.stub(postService, "getIfUserLikesPostByPostId").callsFake((userId: number, postId: number) => Promise.resolve(true));
        const handler = handleGetPostLikeStatusRequest(request as Request, response as Response, postService);
        const result = await handler;

        stubAGetPostUserLikeStatus.restore();
        expect(handler).to.be.instanceOf(Promise);
        expect(stubAGetPostUserLikeStatus.calledOnceWith(dummyPost.user_id, dummyPost.id)).to.be.true;
        expect(result).deep.equal({ like: true });
    });
});