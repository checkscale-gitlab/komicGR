import { Request, Response } from "express";
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { handleGetPostsByUserId } from '../../../WebApi/Controllers/PostsController/index';
import IPostService from "../../../Application.Services/Post/IPostService";
import { PostServiceMOCK } from "../../Mocks/PostServiceMock";
import Post from "../../../Domain/Models/Post/Post";
import { createTestPost } from "../../Helpers/postUtils";

describe('GetPostsByUserIdRequestHandler', () => {
    let request: Partial<Request> = {};
    let response: Partial<Response> = {};
    let postService: IPostService;

    beforeEach(() => {
        request = {
            params: {},
            query: {
                offset: '0',
                limit: '5'
            }
        };
        postService = new PostServiceMOCK();
    });

    it('should return an object with a posts property and as value the posts array response when requesting the posts of a user by his/her id', async () => {
        const dummyPosts = [
            createTestPost(),
            createTestPost(2, 2, [], 1),
        ]
        const userId = String(dummyPosts[0].user_id);
        const stubGetPostsByUserId = sinon.stub(postService, "getPostsByUserId").callsFake((userId: number) => Promise.resolve(dummyPosts));
        request.params!.userId = userId;
        const handler = handleGetPostsByUserId(request as Request, response as Response, postService);
        const result = await handler;

        stubGetPostsByUserId.restore();
        expect(stubGetPostsByUserId.calledOnceWith(dummyPosts[0].user_id)).to.be.true;
        expect(handler).to.be.instanceOf(Promise);
        expect(result).deep.equal({ posts: Post.toPostArrayResponse(dummyPosts) });
    });
});