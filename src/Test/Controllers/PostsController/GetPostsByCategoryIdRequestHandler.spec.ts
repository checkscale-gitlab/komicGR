import { Request, Response } from "express";
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { handleGetPostsByCategoryId } from '../../../WebApi/Controllers/PostsController/index';
import IPostService from "../../../Application.Services/Post/IPostService";
import { PostServiceMOCK } from "../../Mocks/PostServiceMock";
import Post from "../../../Domain/Models/Post/Post";
import { createTestPost } from "../../Helpers/postUtils";

describe('GetPostsByCategoryIdRequestHandler', () => {
    let request: Partial<Request> = {};
    let response: Partial<Response> = {};
    let postService: IPostService;

    beforeEach(() => {
        request = {
            query: {}
        };
        postService = new PostServiceMOCK();
    });

    it('should return an object with a post property and as value the post response', async () => {
        const dummyPosts = [
            createTestPost(),
            createTestPost(2, 1, [], 1),
        ]
        const categoryId = String(dummyPosts[0].category_id);
        const stubGetPostsByCategoryId = sinon.stub(postService, "getPostsByCategoryId").callsFake((categoryId: number) => Promise.resolve(dummyPosts));
        request.query!.categoryId = categoryId;
        request.query!.offset = '0';
        request.query!.limit = '5';
        const handler = handleGetPostsByCategoryId(request as Request, response as Response, postService);
        const result = await handler;

        stubGetPostsByCategoryId.restore();
        expect(stubGetPostsByCategoryId.calledOnceWith(+categoryId)).to.be.true;
        expect(handler).to.be.instanceOf(Promise);
        expect(result).deep.equal({ posts: Post.toPostArrayResponse(dummyPosts) });
    });
});