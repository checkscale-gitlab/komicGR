import { Request, Response } from "express";
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { handleSetLikeRequest } from '../../../WebApi/Controllers/PostsController/index';
import IPostService from "../../../Application.Services/Post/IPostService";
import { PostServiceMOCK } from "../../Mocks/PostServiceMock";
import LikePostDTO from "../../../Domain/Dtos/Post/LikePostDTO";
import User from "../../../Domain/Models/User/User";

describe('SetLikeRequestHandler', () => {
    let request: Partial<Request>;
    let response: Response;
    let postService: IPostService;

    beforeEach(() => {
        request = {
            body: {},
            userFromPassport: {} as User
        };
        postService = new PostServiceMOCK();
    });

    it('should return an object with a success property set to true and a msg set to "Like set to true for post with post id {postId}" when requesting to like a post', async () => {
        const userId = 1;
        const likePostDto: LikePostDTO = { post_id: 1, like: true };
        const stubLikePost = sinon.stub(postService, "likePost").callsFake((userId: number, postId: number, like: boolean) => Promise.resolve(true));
        request.body = likePostDto;
        request.userFromPassport!.id = userId;
        const handler = handleSetLikeRequest(request as Request, response, postService);
        const result = await handler;

        stubLikePost.restore();
        expect(handler).to.be.instanceOf(Promise);
        expect(stubLikePost.calledOnceWith(userId, likePostDto.post_id, likePostDto.like))
        expect(result.success).equal(true);
        expect(result.msg).equal(`Like set to true for post with post id ${likePostDto.post_id}`);
    });
});