import { Request, Response } from "express";
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { handleGetPostLikesRequest } from '../../../WebApi/Controllers/PostsController/index';
import IPostService from "../../../Application.Services/Post/IPostService";
import { PostServiceMOCK } from "../../Mocks/PostServiceMock";

describe('GetPostLikesRequestHandler', () => {
    let request: Partial<Request> = {};
    let response: Partial<Response> = {};
    let postService: IPostService;

    beforeEach(() => {
        request = {
            params: {}
        }
        postService = new PostServiceMOCK();
    });

    it('should return an object with a likes property and as value the post likes', async () => {
        const postId = '1';
        const noOfLikes = 5;
        const stubGetNumberOfPostLikes = sinon.stub(postService, "getNumberOfPostLikes").callsFake((postId: number) => Promise.resolve(noOfLikes));
        request.params!.postId = postId;
        const handler = handleGetPostLikesRequest(request as Request, response as Response, postService);
        const result = await handler;

        stubGetNumberOfPostLikes.restore();
        expect(stubGetNumberOfPostLikes.calledOnceWith(+postId)).to.be.true;
        expect(handler).to.be.instanceOf(Promise);
        expect(result).deep.equal({ likes: noOfLikes });
    });
});