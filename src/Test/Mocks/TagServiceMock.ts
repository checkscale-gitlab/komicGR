import { injectable } from "inversify";

import ITagService from "../../Application.Services/Tag/ITagService";

import Tag from "../../Domain/Models/Tag/Tag";

@injectable()
export class TagServiceMOCK implements ITagService {
  getTags(): Promise<Tag[]> {
    throw new Error("Method not implemented.");
  }
}
