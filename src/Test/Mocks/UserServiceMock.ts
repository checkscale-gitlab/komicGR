import { injectable } from "inversify";

import IUserService from "../../Application.Services/User/IUserService";

import RegisterUserDTO from "../../Domain/Dtos/User/RegisterUserDTO";

@injectable()
export class UserServiceMOCK implements IUserService {
    getAll(): Promise<any> {
        throw new Error("Method not implemented.");
    }    
    getById(id: number): Promise<any> {
        throw new Error("Method not implemented.");
    }
    getByUsername(username: string): Promise<any> {
        throw new Error("Method not implemented.");
    }
    getByEmail(email: string): Promise<any> {
        throw new Error("Method not implemented.");
    }
    getUserByPostId(postId: number): Promise<any> {
        throw new Error("Method not implemented.");
    }
    insertUser(user: RegisterUserDTO): Promise<any> {
        throw new Error("Method not implemented.");
    }
    setProfileImageUrl(userId: number, profileImageUrl: string): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
    setConfirmedEmail(userId: number, confirmed: boolean): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
    comparePassword(candidatePassword: string, hash: string): Promise<any> {
        throw new Error("Method not implemented.");
    }
    updatePassword(userId: number, newPassword: string): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
}