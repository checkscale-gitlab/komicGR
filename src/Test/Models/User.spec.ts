
import * as chai from "chai";
const expect = chai.expect;

import User from "../../Domain/Models/User/User";
import ProfileUserResponseDTO from '../../Domain/Dtos/User/ProfileUserResponseDTO';
import Role from "../../Domain/Models/Role/Role";

function createTestUser(): User {
    return new User(
        1,
        Role.Human,
        "Test User First Name",
        "Test User Last Name",
        "test@test.com",
        "TestUsername",
        "Test Password",
        "Test Job description",
        "Test address",
        1234567890,
        "http://www.someurl.com",
        new Date()
    )
}

function createTestUserWithNoUsername(): User {
    return new User(
        2,
        Role.Human,
        "Test User First Name",
        "Test User Last Name",
        "test@test.com",
        "",
        "Test Password",
        "Test Job description",
        "Test address",
        1234567890,
        "http://www.someurl.com",
        new Date()
    )
}

function createTestUserWithNoPassword(): User {
    return new User(
        3,
        Role.Human,
        "Test User First Name",
        "Test User Last Name",
        "test@test.com",
        "TestUsername",
        "",
        "Test Job description",
        "Test address",
        1234567890,
        "http://www.someurl.com",
        new Date()
    )
}

function createTestUserWithNoEmail(): User {
    return new User(
        4,
        Role.Human,
        "Test User First Name",
        "Test User Last Name",
        "",
        "TestUsername",
        "Test Password",
        "Test Job description",
        "Test address",
        1234567890,
        "http://www.someurl.com",
        new Date()
    )
}

describe('User Model', () => {
    it('should have a confirmed boolean property set to false by default when a user is created', () => {
        let user = createTestUser();
        expect(user.confirmed).to.be.a('boolean');
        expect(user.confirmed).to.be.false;
    })
    it('should throw an error if the username is empty', () => {
        expect(createTestUserWithNoUsername).to.throw("Username cannot be empty");
    });

    it('should throw an error if the password is empty', () => {
        expect(createTestUserWithNoPassword).to.throw("Password cannot be empty");
    });

    it('should throw an error if the username is empty', () => {
        expect(createTestUserWithNoEmail).to.throw("Email cannot be empty");
    });

    it('should have a static function to convert User to ProfileUserResponseDTO', () => {
        expect(User.toProfileResponse).to.be.a('function');
    });

    it('should convert a User to a ProfileUserResponseDTO correctly', () => {
        let user = createTestUser();
        let profile: ProfileUserResponseDTO = User.toProfileResponse(user);
        expect(profile.first_name).to.equal(user.first_name);
        expect(profile.last_name).to.equal(user.last_name);
        expect(profile.role).to.equal(user.role);
        expect(profile.username).to.equal(user.username);
        expect(profile.address).to.equal(user.address);
        expect(profile.email).to.equal(user.email);
        expect(profile.job_desc).to.equal(user.job_desc);
        expect(profile.registration_date).to.equal(user.registration_date.toDateString());
        expect(profile.mobile).to.equal(user.mobile);
        expect(profile.profile_image_url).to.equal(user.profile_image_url);
        expect(profile).not.have.property('password');
    })
});