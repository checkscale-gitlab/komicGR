import * as chai from "chai";
const expect = chai.expect;

import Tag from "../../Domain/Models/Tag/Tag";

const createTag = () => new Tag(1, "tagname");
const createTagWithEmptyName = () => new Tag(1, "");

describe("Tag model", () => {
  it("should create a tag successfuly", () => {
    const tag = createTag();

    expect(tag.id).to.equal(1);
    expect(tag.name).to.equal("tagname");
  });

  it("should throw an error if the post name is empty", () => {
    expect(createTagWithEmptyName).to.throw("A tag name should not be empty");
  });

  it('should have a static function to create Tags from the query result', () => {
    const tag = new Tag(1, "Javascript");
    expect(Tag.createTagsFromQueryResult(`{ "id": "${tag.id.toString()}", "name": "${tag.name}" }`)).to.deep.equal([tag])
  })
});
