import { injectable, Container } from "inversify";
import TYPES from "../../Helpers/DI/Types";

import * as sinon from "sinon";
import * as chai from "chai";
import * as express from "express";
const expect = chai.expect;

import TagService from "../../Application.Services/Tag/TagService";
import ITagRepository from "../../DataAccess/Sql/Repositories/Tag/ITagRepository";
import Tag from "../../Domain/Models/Tag/Tag";

@injectable()
class TagRepositoryyMock implements ITagRepository {
  getTags(): Promise<Tag[]> {
    throw new Error("Method not implemented.");
  }
}

describe("Tag Service", () => {
  let tagService: TagService;
  let tagRepositoryMock: ITagRepository;
  let container: Container;
  const testUserId1 = 1;
  let request: express.Request;

  beforeEach(() => {
    container = new Container();
    container.bind<ITagRepository>(TYPES.ITagRepository).to(TagRepositoryyMock);
    tagRepositoryMock = container.get(TYPES.ITagRepository);
    tagService = new TagService(tagRepositoryMock);
    request = {
      get: (name: string) => {},
      params: {},
      body: {},
    } as express.Request;
  });

  it("should return a Promise with an array of tags, when calling getTags", async () => {
    const mockedTags = [
      { id: 1, name: "tag1" },
      { id: 2, name: "tag2" },
    ];
    const stubHandler = sinon
      .stub(tagRepositoryMock, "getTags")
      .callsFake(() => Promise.resolve(mockedTags));

    const tags = await tagService.getTags();

    stubHandler.restore();
    expect(tags).to.deep.equal(mockedTags);
  });

  it("should call the tagRepository only once, when calling getTags", async () => {
    const mockedTags = [
      { id: 1, name: "tag1" },
      { id: 2, name: "tag2" },
    ];
    const stubHandler = sinon
      .stub(tagRepositoryMock, "getTags")
      .callsFake(() => Promise.resolve(mockedTags));

    const tags = await tagService.getTags();

    stubHandler.restore();
    expect(stubHandler.calledOnce).to.be.true;
    expect(tags).to.deep.equal(mockedTags);
  });
});
