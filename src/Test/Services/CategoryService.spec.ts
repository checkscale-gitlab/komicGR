import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { injectable, Container } from "inversify";
import TYPES from "../../Helpers/DI/Types";

import CategoryService from '../../Application.Services/Category/CategoryService';
import ICategoryRepository from '../../DataAccess/Sql/Repositories/Category/ICategoryRepository';

import AddCategoryDTO from '../../Domain/Dtos/Category/AddCategoryDTO';
import Category from '../../Domain/Models/Category/Category';

@injectable()
class CategoryRepositoryMock implements ICategoryRepository {
    addCategory(newCategory: import("../../Domain/Dtos/Category/AddCategoryDTO").default): Promise<any> {
        throw new Error("Method not implemented.");
    }
    setPostCategory(postId: number, categoryId: number): Promise<any> {
        throw new Error("Method not implemented.");
    }
    getPostCategories(): Promise<import("../../Domain/Models/Category/Category").default[]> {
        throw new Error("Method not implemented.");
    }
}

describe("Category Service", () => {
    let container: Container;
    let categoryService: CategoryService;
    let categoryRepositoryMock: ICategoryRepository;
    const successResultFromPromise = {
        result: "success"
    }
    beforeEach(() => {
        container = new Container();
        container.bind<ICategoryRepository>(TYPES.ICategoryRepository).to(CategoryRepositoryMock);
        categoryRepositoryMock = container.get(TYPES.ICategoryRepository);
        categoryService = new CategoryService(categoryRepositoryMock);
    });

    it("should return a Promise with an object with a property result, when calling add category", async () => {
        const testAddCategoryDTO: AddCategoryDTO = {
            Name: "Test Name",
            Description: "Test Description"
        }

        const stubHandler = sinon.stub(categoryRepositoryMock, "addCategory").callsFake(() => {
            return Promise.resolve(successResultFromPromise);
        });

        const response = await categoryService.addCategory(testAddCategoryDTO);
        stubHandler.restore();

        expect(stubHandler.calledOnceWith(testAddCategoryDTO)).to.be.true;
        expect(response).to.deep.equal(successResultFromPromise);
    });

    it("should return a Promise with an object with a property result, when setting a post's category", async () => {
        const postId = 1;
        const categoryId = 1;

        const stubHandler = sinon.stub(categoryRepositoryMock, "setPostCategory").callsFake(() => {
            return Promise.resolve(successResultFromPromise);
        });

        const response = await categoryService.setPostCategory(postId, categoryId);
        stubHandler.restore();

        expect(stubHandler.calledOnceWith(postId, categoryId)).to.be.true;
        expect(response).to.deep.equal(successResultFromPromise);
    });
    it("should return a Promise with a categories array, when getting all categories", async () => {
        const categories = [new Category(1, "Test Cat 1", "Desc 1"), new Category(2, "Test Cat 2", "Desc 2")]

        const stubHandler = sinon.stub(categoryRepositoryMock, "getPostCategories").callsFake(() => {
            return Promise.resolve(categories);
        });

        const response = await categoryService.getPostCategories();
        stubHandler.restore();

        expect(stubHandler.calledOnce).to.be.true;
        expect(response).to.deep.equal(categories);
        expect(response.length).to.equal(categories.length);
    });
});