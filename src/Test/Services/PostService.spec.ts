import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { injectable, Container } from "inversify";
import TYPES from "../../Helpers/DI/Types";

import PostService from '../../Application.Services/Post/PostService';
import IPostRepository from '../../DataAccess/Sql/Repositories/Post/IPostRepository';

import Post from "../../Domain/Models/Post/Post";
import AddPostDTO from '../../Domain/Dtos/Post/AddPostDTO';
import EditPostDTO from '../../Domain/Dtos/Post/EditPostDTO';
import { createTestPost } from '../Helpers/postUtils';


@injectable()
class PostRepositoryMock implements IPostRepository {
    getAllPosts(): Promise<Post[]> {
        throw new Error("Method not implemented.");
    }
    getPostByPostId(postId: number): Promise<Post> {
        throw new Error("Method not implemented.");
    }
    getPostsByUserId(userId: number): Promise<Post[]> {
        throw new Error("Method not implemented.");
    }
    getPostsByCategoryId(category: number, offset?: number, limit?: number): Promise<Post[]> {
        throw new Error("Method not implemented.");
    }
    addPost(post: import("../../Domain/Dtos/Post/AddPostDTO").default): Promise<any> {
        throw new Error("Method not implemented.");
    }
    likePost(userId: number, postId: number, like: boolean): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
    getIfUserLikesPostByPostId(userId: number, postId: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
    getNumberOfPostLikes(postId: number): Promise<number> {
        throw new Error("Method not implemented.");
    }
    deletePostByPostId(postId: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
    editPost(postId: number, editPostDTO: EditPostDTO): Promise<{ id: number }> {
        throw new Error("Method not implemented.");
    }
    setPostAsUserFavourite(postId: number, userId: number): Promise<{ postId: number }> {
        throw new Error("Method not implemented.");
    }
}

describe("Post Service", () => {
    let container: Container;
    let postService: PostService;
    let postRepositoryMock: IPostRepository;
    const successResultFromPromise = {
        result: "success"
    }
    const testPostId1 = 1;
    const testPostId2 = 2;
    const testUserId1 = 1;
    const post1 = createTestPost(testPostId1, testUserId1);
    const post2 = createTestPost(testPostId2, testUserId1);
    const testPosts = [post1, post2];

    beforeEach(() => {
        container = new Container();
        container.bind<IPostRepository>(TYPES.IPostRepository).to(PostRepositoryMock);
        postRepositoryMock = container.get(TYPES.IPostRepository);
        postService = new PostService(postRepositoryMock);
    });

    it("should return a Promise with a posts array, when calling getPostsByUserId", async () => {
        const stubHandler = sinon.stub(postRepositoryMock, "getPostsByUserId").callsFake(() => {
            return Promise.resolve(testPosts);
        });

        const posts = await postService.getPostsByUserId(post1.user_id);

        expect(posts).to.deep.equal(testPosts);
        expect(posts.length).to.equal(testPosts.length);
    });

    it('should return a Promise with a posts array, when calling getAllPosts', async () => {
        const stubHandler = sinon.stub(postRepositoryMock, "getAllPosts").callsFake(() => {
            return Promise.resolve(testPosts);
        });

        const posts = await postService.getAllPosts();

        expect(posts).to.deep.equal(testPosts);
        expect(posts.length).to.equal(testPosts.length);
    })

    it("should return a Promise with a posts array, when calling getPostsByCategoryId", async () => {
        const stubHandler = sinon.stub(postRepositoryMock, "getPostsByCategoryId").callsFake(() => {
            return Promise.resolve(testPosts);
        });

        const posts = await postService.getPostsByCategoryId(post1.category_id!);

        expect(posts).to.deep.equal(testPosts);
        expect(posts.length).to.equal(testPosts.length);
    });

    it("should return a Promise with an object { result: string}, when calling addPost", async () => {
        const testAddPostDTO: AddPostDTO = {
            user_id: testUserId1,
            title: "Test Add title",
            short_body: "Test Add short",
            body: "Test Add body",
            createdAt: new Date().toLocaleString(),
            imageUrl: 'testImageUrl',
            categoryId: -1
        }

        const stubHandler = sinon.stub(postRepositoryMock, "addPost").callsFake(() => {
            return Promise.resolve(successResultFromPromise);
        });

        const response = await postService.addPost(testAddPostDTO);

        expect(stubHandler.calledOnceWith(testAddPostDTO)).to.be.true;
        expect(response).to.deep.equal(successResultFromPromise);
    });

    it("should return a Promise with a boolean, when setting the user\s post like field", async () => {
        const like = true

        const stubHandler = sinon.stub(postRepositoryMock, "likePost").callsFake(() => {
            return Promise.resolve(like);
        });

        const response = await postService.likePost(testUserId1, testPostId1, like)

        expect(stubHandler.calledOnceWith(testUserId1, testPostId1, like)).to.be.true;
        expect(response).to.deep.equal(like);
    });

    it("should retrun a Promise with a boolean, when getting the user\s post like status", async () => {
        const stubHandler = sinon.stub(postRepositoryMock, "getIfUserLikesPostByPostId").callsFake(() => {
            return Promise.resolve(true);
        });

        const response = await postService.getIfUserLikesPostByPostId(testUserId1, testPostId1)

        expect(stubHandler.calledOnceWith(testUserId1, testPostId1)).to.be.true;
        expect(response).to.be.true;
    });

    it("should retrun a Promise with a number, when getting the post's likes", async () => {
        const stubHandler = sinon.stub(postRepositoryMock, "getNumberOfPostLikes").callsFake(() => {
            return Promise.resolve(5);
        });

        const response = await postService.getNumberOfPostLikes(testPostId1)

        expect(stubHandler.calledOnceWith(testPostId1)).to.be.true;
        expect(response).to.be.equal(5);
    });

    it("should return a Promise with an object with a property id and value the post's id, when calling editPost", async () => {
        const testEditPostDTO: EditPostDTO = {
            title: "Test Add title",
            short_body: "Test Add short",
            body: "Test Add body",
            imageUrl: 'testImageUrl',
            categoryId: -1
        }

        const stubHandler = sinon.stub(postRepositoryMock, "editPost").callsFake(() => {
            return Promise.resolve({ id: 1 });
        });

        const response = await postService.editPost(1, testEditPostDTO);

        expect(stubHandler.calledOnceWith(1, testEditPostDTO)).to.be.true;
        expect(response).to.deep.equal({ id: 1 });
    });

    it("should return a Promise with an object with a property id and value the post's id, when successfully calling editPost", async () => {
        const stubHandler = sinon.stub(postRepositoryMock, "setPostAsUserFavourite").callsFake(() => {
            return Promise.resolve({ id: 1 });
        });

        const response = await postService.setPostAsUserFavourite(1, 100);

        expect(stubHandler.calledOnceWith(1, 100)).to.be.true;
        expect(response).to.deep.equal({ id: 1 });
    });
});