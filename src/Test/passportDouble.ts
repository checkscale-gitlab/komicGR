import { ExtractJwt, Strategy as JwtStrategy, StrategyOptions } from 'passport-jwt';

import User from '../Domain/Models/User/User';
import Role from '../Domain/Models/Role/Role';

const demoUser = new User(1, Role.Human, 't', 't', 't@t.com', 't', 'dascdsc', 'adfa', 'acvavd', 12123, "", new Date());

function passportStrategyDouble(passport) {
    let extractJwt = ExtractJwt;
    let options: StrategyOptions = {
        jwtFromRequest: extractJwt.fromAuthHeaderWithScheme('jwt'),
        secretOrKey: process.env.secret,
        passReqToCallback: true
    };

    passport.use(new JwtStrategy(options, (req, jwt_payload, done) => {
        if (req.body.testRole) {
            demoUser.role = req.body.testRole;
        } else {
            demoUser.role = Role.Human;
        }
        done(null, User.toProfileResponse(demoUser))
    }));
}

export default passportStrategyDouble;