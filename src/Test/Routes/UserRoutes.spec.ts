import { Request, Response } from 'express';
import { Container, injectable } from 'inversify';
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import TYPES from '../../Helpers/DI/Types';
import { UserRouteV1 } from '../../WebApi/Routes/v1/User/UserRoutes';
import * as ErrorHandler from '../../WebApi/Routes/v1/utils';
import { UserServiceMOCK } from '../Mocks/UserServiceMock';
import IUserService from '../../Application.Services/User/IUserService';
import INodeMailerWrapper from '../../Helpers/NodeMailer/INodeMailerWrapper';
import User from '../../Domain/Models/User/User';
import Role from '../../Domain/Models/Role/Role';
import ProfileUserResponseDTO from '../../Domain/Dtos/User/ProfileUserResponseDTO';
import * as GetAllUserHandler from '../../WebApi/Controllers/UserController/GetAllUsersRequestHandler';
import * as GetUserByIdHandler from '../../WebApi/Controllers/UserController/GetUserByIdHandler';
import * as GetUserByPostIdHandler from '../../WebApi/Controllers/UserController/GetUserByPostIdHandler';
import * as RegisterRouteHandler from '../../WebApi/Controllers/UserController/RegisterRouteHandler';
import * as ProfileRouteHandler from '../../WebApi/Controllers/UserController/ProfileRouteHandler';
import * as UploadProfileImageRouteHandler from '../../WebApi/Controllers/UserController/UploadProfileImageRouteHandler';
import * as UploadProfileImageUrlHandler from '../../WebApi/Controllers/UserController/UploadProfileImageUrlRouteHandler';
import * as AuthenticateRouteHandler from '../../WebApi/Controllers/UserController/AuthenticateRouteHandler';
import * as ResetPasswordHandler from '../../WebApi/Controllers/UserController/ResetPasswordHandler';
import * as SendEmailRequestHandler from '../../WebApi/Controllers/UserController/SendEmailRequestHandler';
import * as ConfirmEmailRequestHandler from '../../WebApi/Controllers/UserController/ConfirmEmailRequestHandler';
// import RegisterUserDTO from '../../Domain/Dtos/User/RegisterUserDTO';

function createTestUser(): User {
  return new User(
    1,
    Role.Human,
    'first_name',
    'last_name',
    'test@test.com',
    'test username',
    'test pass',
    'test job_desc',
    'test address',
    111111111,
    'http://www.someurl.com',
    new Date()
  );
}

function createTestProfileUserResponseDTO(): ProfileUserResponseDTO {
  return {
    id: 1,
    role: Role.Human,
    first_name: 'vasdv',
    last_name: 'vsdvs',
    email: 'cscs@csdcs.cds',
    username: 'tergse',
    job_desc: 'afaaf',
    address: '1111',
    mobile: 11111111,
    profile_image_url: '',
    registration_date: ''
  };
}

// function createRegisterUserDTO(): RegisterUserDTO {
//     const registerUserDTO = {
//         "first_name": "test",
//         "last_name": "test last",
//         "email": "test@test.com",
//         "username": "test username",
//         "password": "test pass",
//         "job_desc": "test job_desc",
//         "address": "test address",
//         "mobile": 111111111
//     };
//     return registerUserDTO;
// }

@injectable()
class NodeMailerWrapperDouble implements INodeMailerWrapper {
  getTransporter() {
    return null;
  }
  sendEmail() {
    // Empty body
  }
}

describe('User Routes', () => {
  let userService: IUserService;
  let nodeMailerWrapper: INodeMailerWrapper;
  let jwt: () => Record<string, never>;
  let userRoute: UserRouteV1;
  let container: Container;
  const request: Partial<Request> = {};
  const response: Partial<Response> = {
    send: (body?) => body,
    status: () => response as Response,
    redirect: () => {
      // just to avoid the warning
    }
  };
  let sandbox: sinon.SinonSandbox;

  beforeEach(() => {
    sandbox = sinon.createSandbox();
    container = new Container();
    container.bind<IUserService>(TYPES.IUserService).to(UserServiceMOCK);
    container.bind<unknown>(TYPES.JWT).toConstantValue({
      sign: () => {
        // nothing
      }
    });
    jwt = container.get(TYPES.JWT);
    userService = container.get(TYPES.IUserService);
    nodeMailerWrapper = new NodeMailerWrapperDouble();
    userRoute = new UserRouteV1(
      userService,
      nodeMailerWrapper,
      jwt,
      {},
      {},
      {}
    );
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('/route', () => {
    it("should return a response with an array with each user's profile response", async () => {
      const user1 = createTestUser();
      const testUsers = [user1];

      const stubHandler = sandbox
        .stub(GetAllUserHandler, 'handleGetAllUsersRequest')
        .callsFake(() =>
          Promise.resolve(User.toProfileResponseArray(testUsers))
        );
      const spySend = sandbox.spy(response, 'send');

      await userRoute.getAllUsers(request as Request, response as Response);

      stubHandler.restore();
      spySend.restore();
      expect(stubHandler.calledOnceWith(request, response, userService)).to.be
        .true;
      expect(spySend.calledOnceWith(User.toProfileResponseArray(testUsers))).to
        .be.true;
    });
  });

  describe('/:id route', () => {
    it("should return a response with the matching user's profile response", async () => {
      const testUser1 = createTestUser();
      const stubHandler = sandbox
        .stub(GetUserByIdHandler, 'handleGetUserById')
        .callsFake(() => {
          return Promise.resolve(User.toProfileResponse(testUser1));
        });
      const spySend = sandbox.spy(response, 'send');

      await userRoute.getUserById(request as Request, response as Response);

      stubHandler.restore();
      spySend.restore();
      expect(
        stubHandler.calledOnceWith(
          request as Request,
          response as Response,
          userService
        )
      ).to.be.true;
      expect(spySend.calledOnceWith(User.toProfileResponse(testUser1))).to.be
        .true;
    });
  });

  describe('/byPostId route', () => {
    it("should return a response with success property true and user property with the user's profile if a user has been found by the handler", async () => {
      const user = createTestUser();
      sandbox
        .stub(GetUserByPostIdHandler, 'handelGetUserByPostId')
        .callsFake(() => {
          return Promise.resolve({
            success: true,
            user: User.toProfileResponse(user)
          });
        });
      const stubSend = sandbox.stub(response, 'send');

      await userRoute.getUserByPostId(request as Request, response as Response);

      expect(
        stubSend.calledOnceWith({
          success: true,
          user: User.toProfileResponse(user)
        })
      ).to.be.true;
    });
  });

  describe('/register route', () => {
    describe('error cases', () => {
      it('should handle the error when the registration fails', async () => {
        const error = {
          errorMessage: 'error',
          stack: 'errorStack'
        };
        sandbox
          .stub(RegisterRouteHandler, 'handleRegisterRequest')
          .throws(error);
        const stubHandleError = sandbox.stub(ErrorHandler, 'handleError');

        await userRoute.registerUser(request as Request, response as Response);

        expect(stubHandleError.calledOnceWith(error, response)).to.be.true;
      });
    });
    describe('success cases', () => {
      it('should return a response with success property true and msg "User registered" and the insertId as userId, when the registration is succeeded', async () => {
        sandbox
          .stub(RegisterRouteHandler, 'handleRegisterRequest')
          .resolves({ success: true, msg: 'User registered', insertId: 1 });
        const spySend = sandbox.spy(response, 'send');

        await userRoute.registerUser(request as Request, response as Response);

        expect(
          spySend.calledOnceWith({
            success: true,
            msg: 'User registered',
            insertId: 1
          })
        ).to.be.true;
      });
      // it('should send a confirmation email after a succesful registration', () => {
      // let registerUserDTO = createRegisterUserDTO();
      // let result = {
      //     insertId: 1
      // }
      // let stubUsername = sinon.stub(mockUserService, "getByUsername").callsFake(() => {
      //     return Promise.resolve(null);
      // });
      // let stubEmail = sinon.stub(mockUserService, "getByEmail").callsFake(() => {
      //     return Promise.resolve(null);
      // });
      // let stubInsertUserId = sinon.stub(mockUserService, "insertUser").callsFake(() => {
      //     return Promise.resolve(result);
      // });
      // let mailOptions:MailOptions = {
      //     from: "",
      //     to: "",
      //     subject: "",
      //     text: "",
      //     html: ""
      // }
      // let stubNodeMailerWrapperSendMail = sinon.stub(mockNodeMailerWrapper, 'sendEmail').callsFake((mailOptions) => {
      //     return;
      // })

      // chai.request(server)
      //     .post('/users/register')
      //     .send(registerUserDTO)
      //     .end((err, res) => {
      //         stubNodeMailerWrapperSendMail.calledOnce.should.be.true;
      //         stubNodeMailerWrapperSendMail.calledWith(mailOptions);
      //         stubNodeMailerWrapperSendMail.restore();
      //         stubEmail.restore();
      //         stubUsername.restore();
      //         stubInsertUserId.restore();
      //         res.should.have.status(200);
      //         res.body.should.be.a('object');
      //         res.body.should.have.property('success', true);
      //         res.body.should.have.property('msg', "User registered");
      //         res.body.should.have.property('userId', result.insertId);
      //     });
      // });
    });
  });

  describe('/uploadProfileImageUrl route', () => {
    describe('success cases', () => {
      it('should return the response with success property true and and msg "Profile Image Url added successfully", when setting the img url successfuly', async () => {
        const stubHandler = sandbox
          .stub(
            UploadProfileImageUrlHandler,
            'handleUploadProfileImageUrlRequest'
          )
          .callsFake(() => {
            return Promise.resolve({
              success: true,
              msg: 'Profile Image Url added succesfully'
            });
          });
        const spySend = sandbox.spy(response, 'send');

        await userRoute.uploadProfileImageUrl(
          request as Request,
          response as Response
        );

        stubHandler.restore();
        spySend.restore();
        expect(stubHandler.calledOnceWith(request, response, userService)).to.be
          .true;
        expect(
          spySend.calledOnceWith({
            success: true,
            msg: 'Profile Image Url added succesfully'
          })
        ).to.be.true;
      });
    });
    describe('error cases', () => {
      it('should return the response with success property false and and msg "profile image url is not set", if the user\'s profile image url is not set', async () => {
        const stubHandler = sandbox
          .stub(
            UploadProfileImageUrlHandler,
            'handleUploadProfileImageUrlRequest'
          )
          .callsFake(() => {
            return Promise.resolve({
              success: false,
              msg: 'profile image url is not set'
            });
          });
        const spySend = sandbox.spy(response, 'send');

        await userRoute.uploadProfileImageUrl(
          request as Request,
          response as Response
        );

        stubHandler.restore();
        spySend.restore();
        expect(stubHandler.calledOnceWith(request, response, userService)).to.be
          .true;
        expect(
          spySend.calledOnceWith({
            success: false,
            msg: 'profile image url is not set'
          })
        ).to.be.true;
      });
    });
  });

  describe('/uploadImage route', () => {
    describe('success cases', () => {
      it('should return the response with success property true and and msg "Profile Image added succesfully", if the user\'s profile image and the path to the db are set succesfully', async () => {
        const stubHandler = sandbox
          .stub(
            UploadProfileImageRouteHandler,
            'handleUploadProfileImageRequest'
          )
          .callsFake(() => {
            return Promise.resolve({
              success: true,
              msg: 'Profile Image added succesfully'
            });
          });
        const spySend = sandbox.spy(response, 'send');

        await userRoute.uploadImage(request as Request, response as Response);

        stubHandler.restore();
        spySend.restore();
        expect(
          stubHandler.calledOnceWithExactly(
            request,
            response,
            userService,
            {},
            {},
            {}
          )
        ).to.be.true;
        expect(
          spySend.calledOnceWith({
            success: true,
            msg: 'Profile Image added succesfully'
          })
        ).to.be.true;
      });
    });
    describe('error cases', () => {
      it("should return the response with the errrorMessage property and the errorStackTace, if the user's image url uploads fails", async () => {
        const stubHandler = sandbox
          .stub(
            UploadProfileImageRouteHandler,
            'handleUploadProfileImageRequest'
          )
          .callsFake(() => {
            return Promise.resolve({
              errorMessage: 'ErrorMessage',
              errorStackTrace: 'ErrorStackTrace'
            });
          });
        const spySend = sandbox.spy(response, 'send');

        await userRoute.uploadImage(request as Request, response as Response);

        stubHandler.restore();
        spySend.restore();
        expect(
          stubHandler.calledOnceWithExactly(
            request,
            response,
            userService,
            {},
            {},
            {}
          )
        ).to.be.true;
        expect(
          spySend.calledOnceWith({
            errorMessage: 'ErrorMessage',
            errorStackTrace: 'ErrorStackTrace'
          })
        ).to.be.true;
      });
    });
  });

  describe('/authenticate user route', () => {
    describe('error cases', () => {
      it('it should return a response with success property false and msg "User doesn\'t exist" if the username is wrong', async () => {
        const stubHandler = sandbox
          .stub(AuthenticateRouteHandler, 'handleAuthenticateRequest')
          .callsFake(() => {
            return Promise.resolve({
              success: false,
              msg: "User doesn't exist"
            });
          });
        const spySend = sandbox.spy(response, 'send');

        await userRoute.authenticateUser(
          request as Request,
          response as Response
        );

        stubHandler.restore();
        spySend.restore();
        expect(stubHandler.calledOnceWith(request, response, userService)).to.be
          .true;
        expect(
          spySend.calledOnceWith({ success: false, msg: "User doesn't exist" })
        ).to.be.true;
      });
      it('it should return a response with success property false and msg Wrong Credentials if the email is confirmed and the password is wrong', async () => {
        const stubHandler = sandbox
          .stub(AuthenticateRouteHandler, 'handleAuthenticateRequest')
          .callsFake(() => {
            return Promise.resolve({
              success: false,
              msg: 'Wrong Credentials'
            });
          });
        const spySend = sandbox.spy(response, 'send');

        await userRoute.authenticateUser(
          request as Request,
          response as Response
        );

        stubHandler.restore();
        spySend.restore();
        expect(stubHandler.calledOnceWith(request, response, userService)).to.be
          .true;
        expect(
          spySend.calledOnceWith({ success: false, msg: 'Wrong Credentials' })
        ).to.be.true;
      });
      it('should return a response with success property false and msg please "confirm your email", if the user hasn\'t confirmed his email', async () => {
        const stubHandler = sandbox
          .stub(AuthenticateRouteHandler, 'handleAuthenticateRequest')
          .callsFake(() => {
            return Promise.resolve({
              success: false,
              msg: 'please confirm your email'
            });
          });
        const spySend = sandbox.spy(response, 'send');

        await userRoute.authenticateUser(
          request as Request,
          response as Response
        );

        stubHandler.restore();
        spySend.restore();
        expect(stubHandler.calledOnceWith(request, response, userService)).to.be
          .true;
        expect(
          spySend.calledOnceWith({
            success: false,
            msg: 'please confirm your email'
          })
        ).to.be.true;
      });
    });
    describe('success cases', () => {
      it('it should return a response with success property true and a token property with the token, if the user has confirmed his/her email', async () => {
        const stubHandler = sandbox
          .stub(AuthenticateRouteHandler, 'handleAuthenticateRequest')
          .callsFake(() => {
            return Promise.resolve({
              success: true,
              token: 'JWT ' + 'sampleToken'
            });
          });
        const spySend = sandbox.spy(response, 'send');

        await userRoute.authenticateUser(
          request as Request,
          response as Response
        );

        stubHandler.restore();
        spySend.restore();
        expect(stubHandler.calledOnceWith(request, response, userService)).to.be
          .true;
        expect(
          spySend.calledOnceWith({
            success: true,
            token: 'JWT ' + 'sampleToken'
          })
        ).to.be.true;
      });
    });
  });

  describe('/confirmEmail user route', () => {
    it('it should redirect the user if the confirmation is succeeded', async () => {
      const stubHandler = sandbox
        .stub(ConfirmEmailRequestHandler, 'handleConfirmEmailRequest')
        .callsFake(() => {
          return Promise.resolve(process.env.APPLICATION_URL);
        });
      const spyRedirect = sandbox.spy(response, 'redirect');

      await userRoute.confirmEmail(request as Request, response as Response);

      stubHandler.restore();
      spyRedirect.restore();
      expect(
        stubHandler.calledOnceWith(
          request as Request,
          response as Response,
          userService,
          jwt
        )
      ).to.be.true;
      expect(spyRedirect.calledOnceWith(process.env.APPLICATION_URL)).to.be
        .true;
    });
    it("it should send to the user a message 'Confirm update failed' if the confirmation update fails", async () => {
      const stubHandler = sandbox
        .stub(ConfirmEmailRequestHandler, 'handleConfirmEmailRequest')
        .callsFake(() => {
          return Promise.resolve(process.env.APPLICATION_URL);
        });
      const spyRedirect = sandbox.spy(response, 'redirect');

      await userRoute.confirmEmail(request as Request, response as Response);

      stubHandler.restore();
      spyRedirect.restore();
      expect(
        stubHandler.calledOnceWith(
          request as Request,
          response as Response,
          userService,
          jwt
        )
      ).to.be.true;
      expect(spyRedirect.calledOnceWith(process.env.APPLICATION_URL)).to.be
        .true;
    });
    it("it should send to the user a h1 tag with 'Confirmation Failed...' body if the confirmation fails", async () => {
      const stubHandler = sandbox
        .stub(ConfirmEmailRequestHandler, 'handleConfirmEmailRequest')
        .callsFake(() => {
          return Promise.reject();
        });
      const spySend = sandbox.spy(response, 'send');

      await userRoute.confirmEmail(request as Request, response as Response);

      stubHandler.restore();
      spySend.restore();
      expect(
        stubHandler.calledOnceWith(
          request as Request,
          response as Response,
          userService,
          jwt
        )
      ).to.be.true;
      expect(spySend.calledOnceWith('<h1>Confirmation failed...</h1>')).to.be
        .true;
    });
  });

  describe('/profile user route', () => {
    it("it should return a response with a property user and a value the user's profile", () => {
      const testUser = createTestProfileUserResponseDTO();
      const stubHandler = sandbox
        .stub(ProfileRouteHandler, 'handleProfileRequest')
        .callsFake(() => {
          return { user: testUser };
        });
      const spySend = sandbox.spy(response, 'send');
      userRoute.getUserProfile(request as Request, response as Response);

      stubHandler.restore();
      spySend.restore();
      expect(
        stubHandler.calledOnceWith(request as Request, response as Response)
      ).to.be.true;
      expect(spySend.calledOnceWith({ user: testUser })).to.be.true;
    });
  });

  describe('/sendEmail user route', () => {
    it('should send the response with a property success set to true if the handler returns true', async () => {
      const stubHandler = sandbox
        .stub(SendEmailRequestHandler, 'handleSendEmailRequest')
        .callsFake(() => {
          return Promise.resolve(true);
        });
      const spySend = sandbox.spy(response, 'send');

      await userRoute.sendEmail(request as Request, response as Response);

      stubHandler.restore();
      spySend.restore();
      expect(stubHandler.calledOnce).to.be.true;
      expect(spySend.calledOnceWith({ success: true })).to.be.true;
    });

    it('should send the response with a property success set to false if the handler returns false', async () => {
      const stubHandler = sandbox
        .stub(SendEmailRequestHandler, 'handleSendEmailRequest')
        .callsFake(() => {
          return Promise.resolve(false);
        });
      const spySend = sandbox.spy(response, 'send');

      await userRoute.sendEmail(request as Request, response as Response);

      stubHandler.restore();
      spySend.restore();
      expect(stubHandler.calledOnce).to.be.true;
      expect(spySend.calledOnceWith({ success: false })).to.be.true;
    });

    it('should send a response with status 500 if sending the email failed', async () => {
      const stubHandler = sandbox
        .stub(SendEmailRequestHandler, 'handleSendEmailRequest')
        .throws({ message: 'ErrorMessage', stack: 'ErrorStackTrace' });
      const spySend = sandbox.spy(response, 'send');
      const stubStatus = sandbox.stub(response, 'status').returns(response);

      await userRoute.sendEmail(request as Request, response as Response);

      stubHandler.restore();
      stubStatus.restore();
      spySend.restore();
      expect(stubHandler.calledOnce).to.be.true;
      expect(stubStatus.calledOnceWith(500)).to.be.true;
      expect(
        spySend.calledOnceWith({
          message: 'ErrorMessage',
          stack: 'ErrorStackTrace'
        })
      ).to.be.true;
    });
  });

  describe('/resetPassword user route', () => {
    it('should return a response with a property success set to true if the handler returns true', async () => {
      const stubHandler = sandbox
        .stub(ResetPasswordHandler, 'handleResetPasswordRequest')
        .callsFake(() => {
          return Promise.resolve({ success: true });
        });
      const spySend = sandbox.spy(response, 'send');

      await userRoute.resetPassword(request as Request, response as Response);

      stubHandler.restore();
      spySend.restore();
      expect(stubHandler.calledOnce).to.be.true;
      expect(spySend.calledOnceWith({ success: true })).to.be.true;
    });

    it('should return a response with a property success set to false if the handler returns false', async () => {
      const stubHandler = sandbox
        .stub(ResetPasswordHandler, 'handleResetPasswordRequest')
        .callsFake(() => {
          return Promise.resolve({ success: false });
        });
      const spySend = sandbox.spy(response, 'send');

      await userRoute.resetPassword(request as Request, response as Response);

      stubHandler.restore();
      spySend.restore();
      expect(stubHandler.calledOnce).to.be.true;
      expect(spySend.calledOnceWith({ success: false })).to.be.true;
    });

    it('should send a response with status 500 if resetting the password failed', async () => {
      const stubHandler = sandbox
        .stub(ResetPasswordHandler, 'handleResetPasswordRequest')
        .throws({ message: 'ErrorMessage', stack: 'ErrorStackTrace' });
      const spySend = sandbox.spy(response, 'send');
      const stubStatus = sandbox.stub(response, 'status').returns(response);

      await userRoute.resetPassword(request as Request, response as Response);

      stubHandler.restore();
      spySend.restore();
      expect(stubHandler.calledOnce).to.be.true;
      expect(stubStatus.calledOnceWith(500)).to.be.true;
      expect(
        spySend.calledOnceWith({
          message: 'ErrorMessage',
          stack: 'ErrorStackTrace'
        })
      ).to.be.true;
    });
  });
});
