import { Request, Response } from 'express';
import { Container, injectable } from 'inversify';
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import TYPES from '../../Helpers/DI/Types';
import { CategoryRoute } from '../../WebApi/Routes/v1/CategoryRoutes';
import * as ErrorHandler from '../../WebApi/Routes/v1/utils';
import ICategoryService from '../../Application.Services/Category/ICategoryService';
import IPostService from '../../Application.Services/Post/IPostService';
import { PostServiceMOCK } from '../Mocks/PostServiceMock';
import Category from '../../Domain/Models/Category/Category';
import * as AddCategoryHandler from '../../WebApi/Controllers/CategoryController/AddCategoryHandler';
import * as GetPostCategoriesHandler from '../../WebApi/Controllers/CategoryController/GetPostCategoriesHandler';
import * as SetPostCategoryHandler from '../../WebApi/Controllers/CategoryController/SetPostCategoryHandler';
@injectable()
class CategoryServiceMOCK implements ICategoryService {
  addCategory(): Promise<unknown> {
    throw new Error('Method not implemented.');
  }
  setPostCategory(): Promise<unknown> {
    throw new Error('Method not implemented.');
  }
  getPostCategories(): Promise<Category[]> {
    throw new Error('Method not implemented.');
  }
}

describe('Category routes', () => {
  let categoryService: ICategoryService;
  let postService: IPostService;
  let categoryRoute: CategoryRoute;
  let container: Container;
  const request: Partial<Request> = {};
  let response: Partial<Response>;
  let sandbox: sinon.SinonSandbox;

  beforeEach((done) => {
    sandbox = sinon.sandbox.create();
    response = {
      send: (body?) => body,
      status: () => ({} as Response),
      redirect: () => {
        // Empty body
      }
    };
    container = new Container();
    container
      .bind<ICategoryService>(TYPES.ICategoryService)
      .to(CategoryServiceMOCK);
    container.bind<IPostService>(TYPES.IPostService).to(PostServiceMOCK);
    categoryService = container.get(TYPES.ICategoryService);
    postService = container.get(TYPES.IPostService);
    categoryRoute = new CategoryRoute(categoryService, postService);
    done();
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('success cases', () => {
    it('should return a response with success property true and msg "Category added", when adding a category succeeds', async () => {
      const stubHandler = sandbox
        .stub(AddCategoryHandler, 'handleAddCategory')
        .callsFake(() => {
          return Promise.resolve({ success: true, msg: 'Category Added' });
        });
      const spySend = sandbox.spy(response, 'send');

      await categoryRoute.addCategory(request as Request, response as Response);

      expect(stubHandler.calledOnceWith(request, response, categoryService)).to
        .be.true;
      expect(spySend.calledOnceWith({ success: true, msg: 'Category Added' }))
        .to.be.true;
    });
    it('should return a response with success property true and msg "Category set", when setting a category succeeds', async () => {
      const stubHandler = sandbox
        .stub(SetPostCategoryHandler, 'handleSetPostCategory')
        .callsFake(() => {
          return Promise.resolve({ success: true, msg: 'Category set' });
        });
      const spySend = sandbox.spy(response, 'send');

      await categoryRoute.setCategoryToPost(
        request as Request,
        response as Response
      );

      expect(
        stubHandler.calledOnceWith(
          request as Request,
          response,
          categoryService
        )
      ).to.be.true;
      expect(spySend.calledOnceWith({ success: true, msg: 'Category set' })).to
        .be.true;
    });
    it('should return a response with success property true and a categories property with all categories', async () => {
      const testCategories = [
        new Category(1, '1', '1111'),
        new Category(2, '2', '2222'),
        new Category(3, '3', '3333')
      ];
      const stubHandler = sandbox
        .stub(GetPostCategoriesHandler, 'handleGetPostCategories')
        .callsFake(() => {
          return Promise.resolve({ success: true, categories: testCategories });
        });
      const spySend = sandbox.spy(response, 'send');

      await categoryRoute.getPostCategories(
        request as Request,
        response as Response
      );

      expect(
        stubHandler.calledOnceWith(
          request as Request,
          response,
          categoryService
        )
      ).to.be.true;
      expect(
        spySend.calledOnceWith({ success: true, categories: testCategories })
      ).to.be.true;
    });
  });
  describe('error cases', () => {
    it('should handle the error "You are not authorized for this action", when a user with the role Human tries to add a category', async () => {
      sandbox
        .stub(SetPostCategoryHandler, 'handleSetPostCategory')
        .throws({ message: 'You are not authorized for this action' });
      const stubHandleError = sandbox.stub(ErrorHandler, 'handleError');

      await categoryRoute.setCategoryToPost(
        request as Request,
        response as Response
      );

      expect(
        stubHandleError.calledOnceWith(
          { message: 'You are not authorized for this action' },
          response
        )
      ).to.be.true;
    });
  });
});
