import RegisterUserDTO from "../../../../Domain/Dtos/User/RegisterUserDTO";
import { QueryResponse } from "../../../../Domain/Dtos/QueryResponse";

interface IUserRepository {
    getAll(): Promise<any>;
    getById(id: number): Promise<any>;
    getByUsername(username: string): Promise<any>;
    getByEmail(email: string): Promise<any>;
    getUserByPostId(postId: number): Promise<any>;
    insertUser(user: RegisterUserDTO): Promise<QueryResponse>;
    setConfirmedEmail(userId: number, confirmed: boolean): Promise<boolean>;
    setProfileImageUrl(userId: number, profileImageUrl: string): Promise<boolean>;
    updatePassword(userId: number, newPassword: string): Promise<boolean>;
}

export default IUserRepository;