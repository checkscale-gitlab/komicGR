import * as mysql from 'mysql';
import IPostRepository from './IPostRepository';
import Post from '../../../../Domain/Models/Post/Post';
import AddPostDTO from '../../../../Domain/Dtos/Post/AddPostDTO';
import IUnitOfWork from '../../../../UnitOfWork/IUnitOfWork';
import { inject, injectable } from 'inversify';
import TYPES from '../../../../Helpers/DI/Types';
import { QueryResponse } from '../../../../Domain/Dtos/QueryResponse';
import EditPostDTO from '../../../../Domain/Dtos/Post/EditPostDTO';

@injectable()
class PostRepository implements IPostRepository {
  constructor(@inject(TYPES.IUnitOfWork) private uow: IUnitOfWork) {}

  async getPostByPostId(postId: number): Promise<Post> {
    let sql = `SELECT 
                p.id, p.title, p.short_body, p.body, SUM(ul.like) As likes, p.user_id, p.created_at, p.image_url, u.profile_image_url, pc.category_id,
                (SELECT GROUP_CONCAT('{ "id":"', id, '", "name": "', name, '" }' SEPARATOR '##') FROM tags
                LEFT JOIN posts_tags pt ON pt.tag_id = tags.id
                WHERE post_id = p.id) As tags
            FROM posts p 
            LEFT JOIN user_likes ul ON p.id = ul.post_id
            LEFT JOIN posts_categories pc ON p.id = pc.post_id 
            LEFT JOIN users u ON p.user_id = u.id 
            WHERE p.id = ?
            limit 1`;
    const inserts = [postId];
    sql = mysql.format(sql, inserts);
    const query = await this.uow.query(sql);
    const result = query[0];

    if (result?.id != null) {
      const post = Post.createPostFromQueryResult(result);

      return post;
    }

    throw new Error("Post doesn't exist");
  }

  async getPostsByUserId(
    userId: number,
    offset?: number,
    limit?: number
  ): Promise<Post[]> {
    const defaultOffset = offset || 0;
    const defaultLimit = limit || 6;

    let sql = `SELECT 
                        p.id, p.title, p.short_body, p.body, p.user_id, SUM(ul.Like) As likes, p.created_at, p.image_url, pc.category_id, u.profile_image_url,
                        (SELECT GROUP_CONCAT('{ "id":"', id, '", "name": "', name, '" }' SEPARATOR '##') FROM tags
                        LEFT JOIN posts_tags pt ON pt.tag_id = tags.id
                        WHERE post_id = p.id) As tags
                    FROM posts p 
                    LEFT JOIN users u
                    ON p.user_id = u.id
                    LEFT JOIN user_likes ul 
                    ON p.id = ul.post_id
                    LEFT JOIN posts_categories pc
                    ON pc.post_id = p.id
                    WHERE p.user_id = ? 
                    GROUP BY p.id
                    LIMIT ?
                    OFFSET ?`;
    const inserts = [userId, defaultLimit, defaultOffset];
    sql = mysql.format(sql, inserts);

    const results = await this.uow.query(sql);

    const posts: Post[] = [];

    for (const result of results) {
      const post = Post.createPostFromQueryResult(result);
      posts.push(post);
    }

    return posts;
  }

  async getAllPosts(): Promise<Post[]> {
    const sql = `SELECT 
                        p.id, p.title, p.short_body, p.body, p.user_id, SUM(ul.Like) As likes, p.created_at, p.image_url, pc.category_id, u.profile_image_url,
                        (SELECT GROUP_CONCAT('{ "id":"', id, '", "name": "', name, '" }' SEPARATOR '##') FROM tags
                        LEFT JOIN posts_tags pt ON pt.tag_id = tags.id
                        WHERE post_id = p.id) As tags
                    FROM posts p 
                    LEFT JOIN user_likes ul 
                    ON p.id = ul.post_id
                    LEFT JOIN users u
                    ON p.user_id = u.id
                    LEFT JOIN posts_categories pc
                    ON p.id = pc.post_id
                    GROUP BY p.id
                    ORDER BY created_at DESC`;
    const formattedSql = mysql.format(sql, []);

    const results = await this.uow.query(formattedSql);

    const posts: Post[] = [];

    for (const result of results) {
      const post = Post.createPostFromQueryResult(result);
      posts.push(post);
    }

    return posts;
  }

  async getPostsByCategoryId(
    categoryId: number,
    offset?: number,
    limit?: number
  ): Promise<Post[]> {
    const defaultOffset = offset || 0;
    const defaultLimit = limit || 6;

    let sql = `SELECT 
                        p.id, p.title, p.short_body, p.body, p.user_id, SUM(ul.Like) As likes, p.created_at, p.image_url, pc.category_id, u.profile_image_url,
                        (SELECT GROUP_CONCAT('{ "id":"', id, '", "name": "', name, '" }' SEPARATOR '##') FROM tags
                        LEFT JOIN posts_tags pt ON pt.tag_id = tags.id
                        WHERE post_id = p.id) As tags
                    FROM posts p 
                    LEFT JOIN users u
                    ON p.user_id = u.id
                    LEFT JOIN user_likes ul 
                    ON p.id = ul.post_id
                    LEFT JOIN posts_categories pc
                    ON p.id = pc.post_id
                    WHERE pc.category_id = ?
                    GROUP BY p.id
                    ORDER BY created_at DESC
                    LIMIT ?
                    OFFSET ?`;

    const inserts = [categoryId, defaultLimit, defaultOffset];
    sql = mysql.format(sql, inserts);

    const results = await this.uow.query(sql);

    const posts: Post[] = [];

    for (const result of results) {
      if (result?.id) {
        const post = Post.createPostFromQueryResult(result);

        posts.push(post);
      }
    }

    return posts;
  }

  async addPost(newPost: AddPostDTO): Promise<QueryResponse> {
    let insertPostResult: QueryResponse;

    try {
      await this.uow.beginTransaction();
      const sql = this.insertPost(newPost);
      const result = await this.uow.query(sql);
      insertPostResult = result;
      const sqlToSetPostCategory = this.getUpdatePostCategoriesSqlQuery(
        newPost,
        result
      );
      await this.uow.query(sqlToSetPostCategory);
      await this.uow.commit();

      return insertPostResult;
    } catch (error) {
      await this.uow.rollback();

      throw error;
    }

    // this.uow.beginTransaction();
    // .then(() => this.insertPost(newPost))
    // .then(sql => this.uow.query(sql))
    // .then((result) => {
    //     insertPostResult = result

    //     return this.getUpdatePostCategoriesSqlQuery(newPost, result);
    // })
    // .then((sqlToSetPostCategory) => this.uow.query(sqlToSetPostCategory))
    // .then(() => {
    //     this.uow.commit();

    //     return insertPostResult;
    // })
    // .catch((errorFromSql) => {
    //     this.uow.rollback()
    //         .catch(error => {
    //             console.log(error);
    //             throw error
    //         });
    // })
  }

  async likePost(
    userId: number,
    postId: number,
    like: boolean
  ): Promise<boolean> {
    let sql =
      'INSERT INTO `user_likes` (`user_id`, `post_id`, `like`) values(?,?,?) ON DUPLICATE KEY UPDATE `like` = ?';
    const inserts = [userId, postId, like, like];
    sql = mysql.format(sql, inserts);

    try {
      await this.uow.beginTransaction();
      const result = await this.uow.query(sql);
      this.uow.commit();

      return result.affectedRows > 0;
    } catch (error) {
      await this.uow.rollback();
      throw error;
    }
  }

  getIfUserLikesPostByPostId(userId: number, postId: number): Promise<boolean> {
    let sql =
      'SELECT `like` FROM `user_likes` WHERE user_id = ? AND `post_id` = ? limit 1';
    const inserts = [userId, postId];
    sql = mysql.format(sql, inserts);

    return this.uow
      .query(sql)
      .then((result) => {
        if (result.length === 0 || !result[0].Like) return false;
        return true;
      })
      .catch((err) => {
        throw err;
      });
  }

  getNumberOfPostLikes(postId: number): Promise<number> {
    let sql =
      'SELECT COUNT(*) FROM `user_likes` WHERE post_id = ? AND `like` IS TRUE';
    const inserts = [postId];
    sql = mysql.format(sql, inserts);

    return this.uow
      .beginTransaction()
      .then(() => this.uow.query(sql))
      .then((result) => {
        this.uow.commit();

        return result[0]['COUNT(*)'];
      })
      .catch((err) => {
        this.uow.rollback();
        throw err;
      });
  }

  async deletePostByPostId(postId: number): Promise<boolean> {
    let sql = 'DELETE FROM `posts` WHERE id = ?';
    const inserts = [postId];
    sql = mysql.format(sql, inserts);

    try {
      await this.uow.beginTransaction();
      await this.uow.query(sql);

      this.uow.commit();

      return true;
    } catch (error) {
      return false;
    }
  }

  async editPost(postId, editPostDTO: EditPostDTO): Promise<{ id: number }> {
    let sql =
      'UPDATE posts SET title = ?, short_body = ?, body = ?, image_url = ?  WHERE id = ?';
    const inserts = [
      editPostDTO.title,
      editPostDTO.short_body,
      editPostDTO.body,
      editPostDTO.imageUrl,
      postId
    ];
    let updatePostsCategoriesSql =
      'UPDATE posts_categories SET category_id = ? WHERE post_id = ?';
    const updatePostsCategoriesInserts = [editPostDTO.categoryId, postId];
    sql = mysql.format(sql, inserts);
    updatePostsCategoriesSql = mysql.format(
      updatePostsCategoriesSql,
      updatePostsCategoriesInserts
    );

    await this.uow.beginTransaction();
    await this.uow.query(sql);
    await this.uow.query(updatePostsCategoriesSql);

    this.uow.commit();

    return { id: postId };
  }

  async setPostAsUserFavourite(
    postId: number,
    userId: number
  ): Promise<{ postId: number }> {
    const sql =
      'INSERT INTO user_favourite_posts (post_id, user_id) values(?,?)';
    const inserts = [postId, userId];
    const formattedSql = mysql.format(sql, inserts);

    await this.uow.beginTransaction();
    await this.uow.query(formattedSql);

    this.uow.commit();

    return { postId };
  }

  private insertPost(newPost: AddPostDTO): string {
    let sql =
      'INSERT INTO posts (title, short_body, body, likes, user_id, created_at, image_url) values(?,?,?,?,?,?,?)';
    const inserts = [
      newPost.title,
      newPost.short_body,
      newPost.body,
      0,
      newPost.user_id,
      newPost.createdAt,
      newPost.imageUrl
    ];
    sql = mysql.format(sql, inserts);
    return sql;
  }

  private getUpdatePostCategoriesSqlQuery(newPost: AddPostDTO, result): string {
    const postId = result.insertId;

    if (newPost.categoryId == -1) {
      newPost.categoryId = 1;
    }

    let sqlToSetPostCategory =
      'INSERT INTO `posts_categories` (`post_id`, `category_id`) values(?,?) ON DUPLICATE KEY UPDATE `category_id` = ?';
    const insertsNested = [postId, newPost.categoryId, newPost.categoryId];
    return (sqlToSetPostCategory = mysql.format(
      sqlToSetPostCategory,
      insertsNested
    ));
  }
}

export default PostRepository;
