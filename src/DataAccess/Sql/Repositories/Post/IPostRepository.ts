import Post from "../../../../Domain/Models/Post/Post";
import AddPostDTO from "../../../../Domain/Dtos/Post/AddPostDTO";
import { QueryResponse } from "../../../../Domain/Dtos/QueryResponse";
import EditPostDTO from "../../../../Domain/Dtos/Post/EditPostDTO";

interface IPostRepository {
    getAllPosts(): Promise<Post[]>;
    getPostByPostId(postId: number): Promise<Post>;
    getPostsByUserId(userId: number, offset?: number, limit?: number): Promise<Post[]>;
    getPostsByCategoryId(category: number, offset?: number, limit?: number): Promise<Post[]>;
    addPost(post: AddPostDTO): Promise<QueryResponse>;
    likePost(userId: number, postId: number, like: boolean): Promise<boolean>;
    getIfUserLikesPostByPostId(userId: number, postId: number): Promise<boolean>;
    getNumberOfPostLikes(postId: number): Promise<number>;
    deletePostByPostId(postId: number): Promise<boolean>;
    editPost(postId: number, editPostDTO: EditPostDTO): Promise<{ id: number }>;
    setPostAsUserFavourite(postId: number, userId: number): Promise<{ postId: number }>;
}

export default IPostRepository;