import * as mysql from "mysql";
import { inject, injectable } from "inversify";
import ITagRepository from "./ITagRepository";
import TYPES from "../../../../Helpers/DI/Types";
import IUnitOfWork from "../../../../UnitOfWork/IUnitOfWork";
import Tag from "../../../../Domain/Models/Tag/Tag";

@injectable()
class TagRepository implements ITagRepository {
  constructor(@inject(TYPES.IUnitOfWork) private uow: IUnitOfWork) {}

  async getTags(): Promise<Tag[]> {
    let sql = "SELECT * from tags";

    const results: Tag[] = await this.uow.query(sql);

    const tags: Tag[] = [];
    for (const result of results) {
      const tag = new Tag(result.id, result.name);
      tags.push(tag);
    }

    return tags;
  }
}

export default TagRepository;
