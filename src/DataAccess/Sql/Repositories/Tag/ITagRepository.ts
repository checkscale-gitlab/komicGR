import Tag from "../../../../Domain/Models/Tag/Tag";

interface ITagRepository {
  getTags(): Promise<Tag[]>;
}

export default ITagRepository;
