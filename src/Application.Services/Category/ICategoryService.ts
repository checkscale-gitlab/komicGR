import AddCategoryDTO from "../../Domain/Dtos/Category/AddCategoryDTO";
import Category from "../../Domain/Models/Category/Category";

interface ICategoryService {
    addCategory(newCategory: AddCategoryDTO): Promise<any>;
    setPostCategory(postId: number, categoryId: number): Promise<any>;
    getPostCategories(): Promise<Category[]>;
}

export default ICategoryService;