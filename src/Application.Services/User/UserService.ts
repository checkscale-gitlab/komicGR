import { injectable, inject } from 'inversify';
import TYPES from '../../Helpers/DI/Types';

import IUserService from './IUserService';
import IUserRepository from '../../DataAccess/Sql/Repositories/User/IUserRepository';
import BcryptService from '../../Helpers/Bcryptjs/BcryptService';

import User from '../../Domain/Models/User/User';
import RegisterUserDTO from '../../Domain/Dtos/User/RegisterUserDTO';
import { QueryResponse } from '../../Domain/Dtos/QueryResponse';

@injectable()
class UserService implements IUserService {
    
    constructor(
        @inject(TYPES.IUserRepository) private userRepository: IUserRepository,
        @inject(TYPES.BcryptService) private bcryptService: BcryptService
        ) { }

    public getAll(): Promise<User[]> {
        return this.userRepository.getAll();
    }

    public getById(id: number): Promise<User> {
        return this.userRepository.getById(id);
    }

    public getByUsername(username: string): Promise<User> {
        return this.userRepository.getByUsername(username);
    }

    public getByEmail(email: string): Promise<User> {
        return this.userRepository.getByEmail(email);
    }

    public getUserByPostId(postId: number): Promise<User> {
        return this.userRepository.getUserByPostId(postId);
    }

    public async insertUser(user: RegisterUserDTO): Promise<QueryResponse> {
        const salt = await BcryptService.generateSalt(10);
        const hashKey = await BcryptService.createHashKey(user.password, salt);
        user.password = hashKey;

        const insertResult = await this.userRepository.insertUser(user);

        return insertResult;
    }

    public setProfileImageUrl(userId: number, profileImageUrl: string): Promise<boolean> {
        return this.userRepository.setProfileImageUrl(userId, profileImageUrl);
    }

    public setConfirmedEmail(userId: number, confirmed: boolean): Promise<boolean> {
        return this.userRepository.setConfirmedEmail(userId, confirmed);
    }

    public async comparePassword(candidatePassword: string, hash: string): Promise<any> {
        const isMatch = await BcryptService.comparePassword(candidatePassword, hash);

        return isMatch;
    }

    public updatePassword(userId: number, newPassword: string): Promise<boolean> {
        return this.userRepository.updatePassword(userId, newPassword);
    }
}

export default UserService;