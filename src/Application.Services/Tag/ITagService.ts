import Tag from "../../Domain/Models/Tag/Tag";

interface ITagService {
  getTags(): Promise<Tag[]>;
}

export default ITagService;
