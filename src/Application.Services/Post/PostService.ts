import IPostService from "./IPostService";
import Post from "../../Domain/Models/Post/Post";
import AddPostDTO from "../../Domain/Dtos/Post/AddPostDTO";
import IPostRepository from "../../DataAccess/Sql/Repositories/Post/IPostRepository";
import { injectable, inject } from "inversify";
import TYPES from "../../Helpers/DI/Types";
import { QueryResponse } from "../../Domain/Dtos/QueryResponse";
import EditPostDTO from "../../Domain/Dtos/Post/EditPostDTO";

@injectable()
class PostService implements IPostService {

    constructor(@inject(TYPES.IPostRepository) private postRepository: IPostRepository) { }

    getAllPosts(): Promise<Post[]> {
        return this.postRepository.getAllPosts();
    }

    getPostByPostId(postId: number): Promise<Post> {
        return this.postRepository.getPostByPostId(postId);
    }

    getPostsByUserId(userId: number, offset?: number, limit?: number): Promise<Post[]> {
        return this.postRepository.getPostsByUserId(userId, offset, limit);
    }

    getPostsByCategoryId(categoryId: number, offset?: number, limit?: number): Promise<Post[]> {
        return this.postRepository.getPostsByCategoryId(categoryId, offset, limit);
    }

    addPost(newPost: AddPostDTO): Promise<QueryResponse> {
        return this.postRepository.addPost(newPost);
    }

    likePost(userId: number, postId: number, like: boolean): Promise<boolean> {
        return this.postRepository.likePost(userId, postId, like);
    }

    getIfUserLikesPostByPostId(userId: number, postId: number): Promise<boolean> {
        return this.postRepository.getIfUserLikesPostByPostId(userId, postId);
    }

    getNumberOfPostLikes(postId: number): Promise<number> {
        return this.postRepository.getNumberOfPostLikes(postId);
    }

    deletePostById(userId: number, postId: number): Promise<boolean> {
        return this.postRepository.deletePostByPostId(postId);

    }

    editPost(postId: number, editPostDTO: EditPostDTO): Promise<{ id: number }> {
        return this.postRepository.editPost(postId, editPostDTO);
    }

    setPostAsUserFavourite(postId: number, userId: number): Promise<{ postId: number }> {
        return this.postRepository.setPostAsUserFavourite(postId, userId);
    }
}


export default PostService;