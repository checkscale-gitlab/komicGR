# BLOG-API :ledger:

> A REST api written in TypeScript using NodeJS and Express to be consumed by blogs with some social friendly features. This api has been written (mostly) using the TDD methodology.

## MOTIVATION :muscle:

> This has been my side project to experiment with NodeJS and with the backend world in general. The Blog api is built using the [repository and unit of work](https://docs.microsoft.com/en-us/aspnet/mvc/overview/older-versions/getting-started-with-ef-5-using-mvc-4/implementing-the-repository-and-unit-of-work-patterns-in-an-asp-net-mvc-application) pattern. Also in order to adhere to the [SOLID](https://en.wikipedia.org/wiki/SOLID) principles it uses [InversifyJS](https://inversify.io/) in order to implement the dependency injection mechanism. By using the Blog api you will have full control. Of course that comes with more manual work

## TABLE OF CONTENTS :mag_right:

- [Features](#features)
- [Technology Stack](#technology-stack)
- [Prerequisities](#prerequisities)
- [Local Setup](#local-setup-gear)
- [Testing](#testing-fingers_crossed)
- [Build for production](#build-for-production-rocket)
- [DOCKER](#docker-whale)
- [Contributing](#contributing-raised_hands)
- [Support](#support-shaking_hands)
- [Thanks](#thanks-pray)
- [License](#license-memo)

## FEATURES

Features will be documented soon

## TECHNOLOGY STACK

- [Express](https://expressjs.com/)
- [InversifyJS](https://inversify.io/)
- [Swagger](https://swagger.io/)
- [JWT](https://jwt.io/)
- [Mocha](https://mochajs.org/)
- [Sinon](https://sinonjs.org/)
- [Webpack](https://webpack.js.org/)

## PREREQUISITIES

- NodeJS must me [installed](https://nodejs.org/en/download/)
- MariaDB. Check out [this guide](https://github.com/mariadb-developers/mariadb-getting-started) to get started

## LOCAL SETUP :gear:

- Clone this repo
- Create a `.env` file at the root of the project and copy the following with your own values(remember to remove also the brackets)

```
    # DB CONFIGURATION
    DB_HOST={database_host}
    DB_USER={database_user}
    DB_PASS={database_password}
    DB_NAME={database_name}
    secret={jwt_secret}
    confirmEmailSecret={confirm_email_secret}

    # SWAGGER CONTACT INFO
    SWAGGER_CONTACT_NAME={contact_name}
    SWAGGER_CONTACT_MAIL={contact_email}

    # NODEMAILER MAIL OPTIONS
    # for info on this go to https://nodemailer.com/smtp/
    MAIL_HOST={mail_host}
    MAIL_PORT={mail_port}
    MAIL_SECURE={mail_secure}
    MAIL_AUTH_USER={mail_auth_user}
    MAIL_AUTH_PASS={mail_auth_pass}
    MAIL_TLS_REJECTUNAUTHORIZED={mail_tls_reject_unauthorized}

    # WHERE TO REDIRECT WHEN CONFIRMATION OF AN EMAIL FAILS
    APPLICATION_ERROR_PAGE=http://localhost:3000/whoops

    # SENDER OF THE CONFIRMATION OR RESET PASSWORD EMAIL
    FROM_EMAIL='"KomicGR" <info@komic.gr>'

    # THIS IS USED IN THE CONFIRMATION EMAIL
    # SENT TO THE USER UPON REGISTRATION
    BACKEND_URL={api_url}

    # THIS IS USED IN THE RESET LINK THAT IS SENT
    # TO A USER BY EMAIL
    APPLICATION_URL={client_url}

    # A SECRET FOR EXPRESS SESSION
    EXPRESS_SESSION_SECRET={your_secret}

    # OPTIONAL, IF THIS IS NOT PROVIDED
    # IT WILL RUN ON PORT 3001
    PORT={the_port_where_the_api_will_run}
```

- Run `npm install` or `yarn` to install the dependencies
- Run `npm run db-up` or `yarn run db-up` to run the migrations for the database
- Run `npm run dev` or `yarn dev`

The api should be running on `http://localhost:3000` (if you haven't define a port in .env). You will be redirected to `http://localhost:3000/api-docs/v1`. You can also check the `v2` by changing the url to `http://localhost:3001/api-docs/v2`. **_Version 2 is not being developed. It exists as a PoC_**

## TESTING :fingers_crossed:

- Run `npm test` or `yarn test`

<em>Even though I have tried to built this api using TDD unfortunately this is not the case for the repositories. Hopefully tests will be added eventually</em> :disappointed:

## BUILD FOR PRODUCTION :rocket:

- Run `npm build` or `yarn build`
- Set the same environmental variables with the appropriate values in your server
- Deploy / copy the `build` folder in your server
- Install the dependencies with `npm install` or `yarn`
- Start the app with `node app.js`

## DOCKER :whale:

- You can build a docker image of the api using the `Dockerfile`

## CONTRIBUTING :raised_hands:

Contributions will be accepted very soon. Thank you for your interest! :heart:

## SUPPORT :shaking_hands:

- I will be more than happy to help you. You can report an issue using the (issues page)[https://gitlab.com/kormic/komicGR/-/issues]. Please be polite and try to be as descriptive as you can

## THANKS :pray:

This section will be filled soon

## License :memo:

[MIT](https://choosealicense.com/licenses/mit/)
