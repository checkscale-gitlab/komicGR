FROM node:14.1-alpine

RUN apk add --no-cache yarn

WORKDIR /usr/src/app
COPY package.json ./ 
COPY yarn.lock ./
RUN yarn install

ENV PATH="./node_modules/.bin:$PATH"

COPY . ./

EXPOSE 3000

CMD ["yarn", "dev"]